<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
use App\User;
use App\Orderitem;
use Auth;

class SalesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $orders = Orders::with('orderitems')->get();
        return view("sales.orders", compact('orders'));
    }

    public function confirmbyAdmin()
    {
        $orders = Orders::where('approved_status','Confirm_byAdmin');
        return view("sales.orders", compact('orders'));
    }

    public function confirmbyajent()
    {
        $orders = Orders::where('approved_status','Confirm_byAjent');
        return view("sales.orders", compact('orders'));
    }

    public function show($id)
    {
       
       $itemorder = Orders::with('orderitems')->where('id',$id)->orderBy('id', 'DESC')->get();
       return view("sales.orderitems", compact('itemorder'));
    }

    public function update(Request $request ,$id)
    {
        // $order =Orders::where('id',$id)->update(['status' => $request->order_status , 'approved_status' , ]);

        // dd($request->all());
        $order = Orders::find($id);
        $order->status =  $request->order_status;
        $order->approved_status = $request->order_approved_status;
        $order->save();


        return redirect('/admin/orders');
    }

}
