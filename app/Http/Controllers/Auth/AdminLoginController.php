<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Auth;
use App\Admin;
use App\Companies;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordRecoveryEmail;

class AdminLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function showLoginForm(){
        return view ('auth.admin-login');
    }

    public function showCompanyRegisterForm()
    {
        return view('auth.companyregister');
    }

    public function showCompanyRequestForm()
    {
        return view('auth.companyreqeustform');
    }

    public function showPasswordResetForm()
    {
        return view('auth.company-passwordresetform');
    }


    public function registercompany(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'password' => 'min:6',
            'licenseimage' => 'image|mimes:jpeg,png,jpg',
        ]);

        $companymobileandemailcheck = Companies::where('mobile',$request->mobile)->orWhere('email',$request->email)->first();
        if(gettype($companymobileandemailcheck) == "object")
        {
            if($companymobileandemailcheck->accept == 2)
            {
                return redirect('/c/request')->with('companyid',$companymobileandemailcheck->id);
            }
            else if($companymobileandemailcheck->accept == 1)
            {
                return redirect('/c/passwordreset')->with('companyid',$companymobileandemailcheck->id);
            }
        }
        else
        {
            $company = new Companies;
            $company->companyname = $request->companyname;
            $company->personname = $request->personname;
            $company->mobile = $request->mobile;
            $company->telephone = $request->telephone;
            $company->email = $request->email;
            $company->password = md5($request->password);
            $company->address = $request->address;
            $company->city = $request->city;
            $company->licensenumber = $request->licensenumber;
            $company->description = $request->description;

            if($request->hasfile('companylogo'))
            {
                $request->validate([
                    'companylogo' => 'required|image|mimes:jpeg,png,jpg'
                ]);

                $imagename = request()->companylogo->getClientOriginalName();

                request()->companylogo->move(public_path('images/'), $imagename);

                $company->companylogo = $imagename;
            }

            $company->save();

            $registrationsuccess = 'Thank you for your registration. After Admin Approval you will receive control panel';
            return view('auth.companyregistersuccess',compact('registrationsuccess'));


        }
    }

    public function showCompanyRequestFormpost(Request $request)
    {
        $company = Companies::find($request->companyid);
        $company->request = 1;
        $company->save();

        return back()->with('success','Your request have been sent');
    }

    public function  showPasswordResetFormpost(Request $request)
    {
//
        $company = Companies::where('mobile',$request->mobileoremail)->orWhere('email', $request->mobileoremail)->first();

        if(gettype($company) == "object")
        {
            $uniqidStr = md5(uniqid(mt_rand()));

            $link = url("/c/setpassword/".$uniqidStr);

            $data = array(
                'link' => $link
            );

            Mail::to($company->email)->send(new PasswordRecoveryEmail($data));

            $companyFieldsUpdate = Companies::find($company->id);
            $companyFieldsUpdate->passwordrecovery = 1;
            $companyFieldsUpdate->passwordcode = $uniqidStr;
            $companyFieldsUpdate->save();

            return back()->with('success','An email with link has been sent to you.');

        }
        else
        {
            return back()->with('error','Sorry Email not found');
        }
    }

    function setpassword($token)
    {
//        $tokenId = $token;

        $company = Companies::where('passwordcode', $token)->where('passwordrecovery', 1)->first();

        if(gettype($company) == "object")
        {
            return view('auth.copmany_set_password', compact('token'));
        }
        else
        {
            return redirect('/v/login');
        }
    }

    public function setpasswordPost(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6'
        ]);

        $company = Companies::where('passwordcode', $request->tokenid)->first();

        if(gettype($company) == "object")
        {
            $company->password = md5($request->password);
            $company->save();
            return back()->with('success','Your password has been changed');
        }
        else
        {
            return redirect('/v/login');
        }

    }


    public function showCompanyLoginForm()
    {
        return view('auth.company-login');
    }

    public function companyloginpost(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'password' => 'min:6'
        ]);

        $company = Companies::where('email', $request->email)->where('password',md5($request->password))->where('accept',1)->first();

        if(gettype($company) == "object")
        {
            $request->session()->put('companyid', $company->id);
            $request->session()->put('companyname', $company->companyname);
            $request->session()->put('personname', $company->personname);
            $request->session()->put('companylogo', $company->companylogo);


            return redirect('/c/');

        }
        else
        {
            return back()->with('error','Login Failed!');
        }

    }

    
    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);

      // Attempt to log the user in
      if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('admin.dashboard'));
      }

      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors(['password'=>'Authentication Failed']);
    }

    public function logout()
    {
      Auth::guard('admin')->logout();
      return redirect('/admin/login');
    }

    
}
