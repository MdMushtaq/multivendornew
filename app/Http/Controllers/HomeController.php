<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Categories;
use App\subcategories;
use Cart;
use App\products;
use App\Wishlist;
use App\User;
// use App\Session;
use App\Orders;
use App\Orderitem;
use TapPayments\GoSell;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except([
            // 'cartadd'
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orderitem = Orders::with('orderitems')->orderBy('id', 'DESC')->get();

        $wishlist = Wishlist::with('products')->where('user_id',auth()->id())->orderBy('id', 'DESC')->get();
        return view('front-end.customer_dashboard',compact('orderitem','wishlist'));
    }


    public function wishlist($id)
    {
       $wishlist = new Wishlist;
       $wishlist->product_id = $id;
       $wishlist->user_id = auth()->id();
       $wishlist->save();

       return back()->with('success','Successfully Added to Your Wishlist');

    }
    public function checkoutForm()
    {
        $cart =  Cart::content();
        $products = products::whereIn('id', $cart->pluck('id')->toArray())->get();
        // foreach ($products as $product) {
        //     $item = $cart->where('id', $product->id)->first();
        //     $product->qty = $item->qty;
        //     $product->total = $item->qty * $product->price;
        // }
        foreach ($cart as $item) {
            $options = $item->options->toArray();
            unset($options['original_price']);
            $product = $products->where('id', $item->id)->first();
            $item->product = $product;
            $item->total = $item->qty * $item->price;
            $item->attributes = $options;
        }
        $customerinfo = auth()->user();        
        return view('front-end.checkout-form',compact('cart', 'customerinfo'));
        # code...
    }

    public function Checkoutpost(Request $request)
    {
        $cartItems = Cart::content();
        if ($cartItems->count() === 0) {
          return redirect('/')->with('error', 'Your cart is empty!');
        }
        $subTotal = Cart::subtotal();
        $vat = number_format((config('cart.tax') * $subTotal) / 100, 2);
        $total = number_format($subTotal + $vat, 2);

        $order = new Orders;
        $order->user_id = auth()->id();
        $order->shipping_name = $request->shipping_name;
        $order->shipping_phone = $request->shipping_phone;
        $order->shipping_address = $request->shipping_address;
        $order->status = 'Pending';
        $order->approved_status = '';
        $order->vat = $vat;
        $order->total = $total;
        $order->shipping_price = 0;
        $order->subtotal = $subTotal;
        $order->save();

        foreach($cartItems as $item)
        {
            $options = $item->options->toArray();
            $original_price = $options['original_price'];
            unset($options['original_price']);
            $items = new Orderitem;
            $items->order_id = $order->id;
            $items->product_id = $item->id;
            $items->product_name = $item->name;
            $items->product_unit_price = $item->price;
            // $items->product_original_price = $original_price;
            $items->product_quantity = $item->qty;
            $items->product_price = $item->price * $item->qty;
            $items->product_color = implode(', ', $options);
            $items->admin_id = $item->product->admin_id;
            $items->save();
        }        

        Cart::destroy();

        return redirect()->route('checkout.pay', encrypt($order->id));
    }

    public function payView(Request $request, $orderId)
    {
      try {
        $order = Orders::with([
          'orderitems' => function ($q)
          {
            $q->with('product');
          }
        ])->findOrFail(decrypt($orderId));
      } catch (Exception $e) {
        abort(404);
      }
      return view('front-end.transaction', compact('order'));
    }

    public function pay($id)
    {
        $id = decrypt($id);

        $order = Orders::findOrFail($id);
        $customerDetails = User::where('id',$order->user_id)->first();
        // /set yout secret key here
        GoSell::setPrivateKey("sk_test_CceoE46KA3gx9MTNjhRk2uiY");
        
        $charge = GoSell\Charges::create(
            [
              "amount"=> $order->total,
              "currency"=> "KWD",
              "threeDSecure"=> true,
              "save_card"=> false,
              "description"=> "Test Description",
              "statement_descriptor"=> "Sample",
              "metadata"=> [
                "udf1"=> "test 1",
                "udf2"=> "test 2"
              ],
              "reference"=> [
                "transaction"=> "txn_0001",
                "order"=> "ord_0001"
              ],
              "receipt"=> [
                "email"=> false,
                "sms"=> true
              ],
              "customer"=> [
                "first_name"=> $customerDetails->name,
                // "middle_name"=> "test",
                // "last_name"=> "test",
                "email"=> $customerDetails->email,
                "phone"=> [
                  // "country_code"=> "966",
                  "number"=> $customerDetails->phone
                ]
              ],
              "source"=> [
                "id"=> "src_all"
              ],
              "post"=> [
                "url"=> "http://localhost/laravel-saidaliah/"
              ],
              "redirect"=> [
                "url"=> "http://localhost/laravel-saidaliah/"
              ]
            ]
        );
                
        // echo '<pre>';
        // dd($charge);
        return redirect($charge->transaction->url);
    }
    
    public function CustomerDashboard()
    {
       $orderitem = Orders::with('orderitems')->orderBy('id', 'DESC')->get();

       $wishlist = Wishlist::with('products')->where('user_id',auth()->id())->orderBy('id', 'DESC')->get();
        return view('front-end.customer_dashboard',compact('orderitem','wishlist'));
    }
    public function show($id)
    {
       
       $itemorder = Orders::with('orderitems')->where('id',$id)->orderBy('id', 'DESC')->get();
       return view("front-end.cartitems", compact('itemorder'));
    }


}


