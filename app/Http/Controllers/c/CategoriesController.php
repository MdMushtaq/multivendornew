<?php

namespace App\Http\Controllers\c;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\subcategories;
use App\Brands;
use App\products;
use App\Productattributes;

class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        //
        $categories = Categories::where('cat_status',1)->where('company_id',$request->session()->get('companyid'))->get();
        return view('c.product.categories', compact('categories'));
    }

    public function create()
    {
        return view('c.product.addcategory');
    }

    public function store(Request $request)
    {
        request()->validate([
                'cat_image' => 'required',
                'cat_arabic_image' => 'required'
            ],
            [
                'cat_image.required' => 'Imge should be required!',
                'cat_arabic_image.required' => 'Arabic Imge should be required!'
            ]);

        if($request->hasfile('cat_image'))
        {
            foreach($request->file('cat_image') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/categories/', $name);
                $data[] = $name;
            }

        }
        if($request->hasfile('cat_arabic_image'))
        {

            foreach($request->file('cat_arabic_image') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/categories/', $name);
                $data1[] = $name;
            }

        }
        $categories = new Categories();
        $categories->cat_name = $request->name;
        $categories->cat_arabic_name = $request->arabic_name;
        $categories->cat_parent = $request->parent_id;
        $categories->cat_status = $request->status;
        $categories->cat_img = json_encode($data);
        $categories->cat_arabic_img = json_encode($data1);
        $categories->cat_seo_title = $request->seo_title;
        $categories->cat_meta_data = $request->meta;
        $categories->cat_slug = $request->slug;
        $categories->cat_sequence = $request->sequence;
        $categories->company_id = $request->session()->get("companyid");
        $categories->save();

        return redirect('/c/categories')->with('success','Successfully created Categories');
    }

    public function edit(Request $request, $id)
    {
        $categories = Categories::where('company_id',$request->session()->get('companyid'))->where('id',$id)->first();
        return view('c.product.update_category', compact('categories'));
    }

    public function update(Request $request, $id)
    {
        $categories = Categories::where('company_id',$request->session()->get('companyid'))->where('id',$id)->first();
        $categories->cat_name = $request->name;
        $categories->cat_arabic_name = $request->arabic_name;
        $categories->cat_parent = $request->parent_id;
        $categories->cat_status = $request->status;

        if($request->hasfile('cat_image'))
        {
            foreach($request->file('cat_image') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/categories/', $name);
                $data[] = $name;
                $categories->cat_img = json_encode($data);
            }
        }

        if($request->hasfile('cat_arabic_image')){

            foreach($request->file('cat_arabic_image') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/categories/', $name);
                $data1[] = $name;
                $categories->cat_arabic_img = json_encode($data1);
            }
        }

        $categories->cat_seo_title = $request->seo_title;
        $categories->cat_meta_data = $request->meta;
        $categories->cat_slug = $request->slug;
        $categories->cat_sequence = $request->sequence;

        $categories->save();

        return redirect('/c/categories')->with('success','Successfully updated Categories');
    }

    public function categoriesTrash(Request $request)
    {
        //
        $categories = Categories::where('cat_status',0)->where('company_id',$request->session()->get('companyid'))->get();
        return view('c.product.categories', compact('categories'));
    }

    public function subcategories(Request $request)
    {

        $data["subcategories"] = subcategories::where('status','1')->where('company_id',$request->session()->get('companyid'))->get();
        return view('c.product.subcategories', $data);
    }

    function subcategoriesform(Request $request)
    {
        $data["categories"] = Categories::where('company_id',$request->session()->get('companyid'))->get();
        return view('c.product.subcategoriesform', $data);
    }

    function subcategoriesformpost(Request $request)
    {

        $subCategories = new subcategories;

        $subCategories->sub_cat_name = $request->name;
        $subCategories->sub_cat_arabic_name = $request->arabic_name;
        $subCategories->parent_cat_id = $request->parent_cat_id;
        $subCategories->sequence = $request->sequence;
        $subCategories->slug = $request->slug;
        $subCategories->meta_data = $request->meta;
        $subCategories->seo_title = $request->seo_title;
        $subCategories->status = $request->enabled_1;
        $subCategories->company_id = $request->session()->get('companyid');

        $subCategories->save();

        return redirect('c/subcategories')->with('success','Successfully Created Sub Categories');
    }

    function subcategoriesformedit(Request $request, $subcategoryid)
    {
        $categories = new Categories;
        $data["categories"] = Categories::where('company_id',$request->session()->get('companyid'))->get();
        $data["subcategory"] = subcategories::where('company_id', $request->session()->get('companyid'))->where('id', $subcategoryid)->first();
        return view("c.product.subcategoriesformedit", $data);
    }

    function subcategoriesformeditpost(Request $request)
    {
        $subcategoryid = $request->subcategoryid;

        $updateColumns = array(
            'sub_cat_name' => $request->name,
            'sub_cat_arabic_name' => $request->arabic_name,
            'parent_cat_id' => $request->parent_cat_id,
            'sequence' => $request->sequence,
            'slug' => $request->slug,
            'meta_data' => $request->meta,
            'seo_title' => $request->seo_title,
            'status' => $request->enabled_1,
        );

        $subcategories = new subcategories;
        $subcategories::where('id', $subcategoryid)->where('company_id',$request->session()->get('companyid'))->update($updateColumns);
        return redirect('c/subcategories')->with('success','Successfully updated Sub Categories');
    }

    public function brands(Request $request)
    {
        $brands = Brands::where('brands_status',1)->where('company_id',$request->session()->get('companyid'))->get();
        return view('c.product.brands', compact('brands'));
    }

    public function addbrands()
    {
        return view('c.product.brands-form');
    }

    public function storebrands(Request $request)
    {
        request()->validate([
                'image' => 'required|',

            ],
            [
                'image.required' => 'Image feild required!',

            ]);
        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $brands_img=time().$file->getClientOriginalName();
            $file->move(public_path().'/images/brands/', $brands_img);

        }

        $brands = new Brands();
        $brands->brands_name = $request->name;
        $brands->brands_arb_name = $request->arabic_name;
        $brands->brands_img = $brands_img;
        $brands->brands_status = $request->status;
        $brands->brands_seo_title = $request->seo_title;
        $brands->brands_meta_data = $request->meta;
        $brands->brands_slug = $request->slug;
        $brands->brands_sequence = $request->sequence;
        $brands->company_id = $request->session()->get('companyid');
        $brands->save();

        return redirect('/c/brands')->with('success','Successfully created Brands');

    }

    public function editbrand(Request $request, $id)
    {
        $brands = Brands::where('company_id',$request->session()->get('companyid'))->where('id',$id)->first();

        return view('c.product.brandsedit',compact('brands'));
    }

    function updatebrand(Request $request,$id)
    {
        $brands = Brands::where('company_id',$request->session()->get('companyid'))->where('id',$id)->first();
        $brands->brands_name = $request->name;
        $brands->brands_arb_name = $request->arabic_name;

        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $brands_img=time().$file->getClientOriginalName();
            $file->move(public_path().'/images/brands/', $brands_img);
            $brands->brands_img = $brands_img;
        }
        $brands->brands_status = $request->status;
        $brands->brands_seo_title = $request->seo_title;
        $brands->brands_meta_data = $request->meta;
        $brands->brands_slug = $request->slug;
        $brands->brands_sequence = $request->sequence;
        $brands->save();

        return redirect('/c/brands')->with('success','Successfully Update Brands');
    }

    public function trashsubcategories(Request $request)
    {
        $data["subcategories"] = subcategories::where('company_id',$request->session()->get('companyid'))->where('status','0')->get();
        return view('c.product.subcategories', $data);
    }

    public function trashbrand(Request $request)
    {
        $brands = Brands::where('company_id',$request->session()->get('companyid'))->where('brands_status',0)->get();
        return view('c.product.brands', compact('brands'));
    }

    public function deletebrand(Request $request, $id)
    {
        $brands = Brands::where('company_id',$request->session()->get('companyid'))->where('id',$id)->first();
        $brands->delete();

        return back()->with('danger','Successfully Deleted Brands');

    }

    public function subcategories_delet(Request $request, $id)
    {

        $subcategories = subcategories::where('company_id',$request->session()->get('companyid'))->where('id',$id)->first();
        $subcategories->delete();

        return back()->with('danger','Successfully deleted Sub Categories');
    }

    public function delete(Request $request, $id)
    {


        $categories = Categories::where('company_id',$request->session()->get('companyid'))->where('id',$id)->first();

        if($categories->cat_status == 1)
        {
            Categories::where('id',$categories->id)->update(['cat_status' => '0']);
        }
        elseif($categories->cat_status == 0)
        {
            // dd($categories->cat_status);
            $categories->delete();
        }

        return redirect('c/categories')->with('danger','Successfully deleted Categories');
    }

    public function products(Request $request)
    {
        $products = products::with('productQuantity')->where('status',1)->where('company_id',$request->session()->get('companyid'))->orderBy('created_at','desc')->get();

        return view('c.product.products',compact('products'));
    }

    public function addproducts(Request $request)
    {
        $data["categories"] = Categories::where('company_id',$request->session()->get('companyid'))->get();
        $brands = Brands::where('company_id',$request->session()->get('companyid'))->get();

        return view('c.product.products-form', $data,compact('brands'));
    }

    function getsubcategories(Request $request)
    {
        $mainCatId = $request->categoryid;
        $data["subcategories"] = subcategories::where('parent_cat_id', $mainCatId)->where('company_id',$request->session()->get('companyid'))->get();

        echo "<option value=''>SELECT</option>";
        foreach($data["subcategories"] as $subcategory)
        {
            echo "<option value='$subcategory->id'>".$subcategory->sub_cat_name."</option>";
        }
    }

    public function addproductspost(Request $request)
    {

        $request->validate([
            'additionalinformation' => 'max:1000',
            'arabic_additionalinformation' => 'max:1000'
        ]);


        request()->validate([
                'primary_category' => 'required',
                'secondary_category' => 'required',
                'product_img' => 'required',
                'arb_product_img' => 'required'
            ],
            [
                'primary_category.required' => 'Categories feild required!',
                'secondary_category.required' => 'SubCategories feild required!',
                'product_img.required' => 'Product image required',
                'arb_product_img.required' => 'Product arbic image required'
            ]);


        if($request->hasfile('product_img')){

            foreach($request->file('product_img') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/products/', $name);
                $data[] = $name;
            }
        }


        if($request->hasfile('arb_product_img')){

            foreach($request->file('arb_product_img') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/products/', $name);
                $data1[] = $name;
            }
        }

        if(count($request->product_img)>4)
        {
            return back()->with("danger","More than 4 images are not allowed");
        }

        if(count($request->arb_product_img)>4)
        {
            return back()->with("danger","More than 4 images are not allowed");
        }

        // if($request->newarrival)
        // {
        //     $checkNewArrival = products::where('newarrival','on')->get()->count();
        //     if($checkNewArrival >= 15)
        //     {
        //         return back()->with("error","New arrival has limit of 15");
        //     }
        // }
        // else if($request->hotproducts)
        // {
        //     $checkhotproducts = products::where('hotproducts','on')->get()->count();
        //     if($checkhotproducts >= 15)
        //     {
        //         return back()->with("error","Hot products has limit of 15");
        //     }
        // }
        // else if($request->featuredproducts)
        // {
        //     $checkfeaturedproducts = products::where('featuredproducts','on')->get()->count();
        //     if($checkfeaturedproducts >= 15)
        //     {
        //         return back()->with("error","Feature Products has limit of 15");
        //     }
        // }


        $products = new products;

        $products->prod_name = $request->name;
        $products->arabic_name = $request->arabic_name;
        $products->description = $request->description;
        $products->arabic_description = $request->arabic_description;
        $products->model = $request->model;
        $products->search_for_related_product_1 = 'showing null';

        $products->image = json_encode($data);
        $products->arabic_image = json_encode($data1);
        $products->videolink = $request->videolink;
        $products->img_height = '310';

        $products->track_stock = $request->track_stock;;
        $products->available_quantity = $request->available_quantity;
        $products->url_keyword = $request->slug;
        $products->title_tag = $request->seo_title;
        $products->meta_tag = $request->meta;
        $products->specification = $request->specification;
        $products->arabic_specification = $request->arabic_specification;

        $products->additionalinformation = $request->additionalinformation;
        $products->arabic_additionalinformation = $request->arabic_additionalinformation;



        $products->newarrival = $request->newarrival;
        $products->hotproducts = $request->hotproducts;
        $products->featuredproducts = $request->featuredproducts;

        $products->shipping = $request->shippable;
        $products->brands = $request->brand;
        $products->sku = $request->sku;
        $products->weight = $request->weight;
        $products->retail = 'retail';
        $products->status = $request->status;
        $products->price = $request->price_1;
        $products->sale_price = $request->saleprice_1;
        $products->all_offers = "";

        $products->categories_id = $request->primary_category;
        $products->sub_category_id = $request->secondary_category;
        $products->company_id = $request->session()->get('companyid');

        $products->save();



        $maxProductId = $products::max('id');
        $productColor = $request->productcolour;
        $productquantity = $request->quantity;
        $index = 0;
        foreach($productColor as $producttcolor)
        {
            $productAttributes = new Productattributes;
            $productAttributes->colourname = $producttcolor;
            $productAttributes->quantity = $productquantity[$index];
            $productAttributes->productid = $maxProductId;
            $productAttributes->company_id = $request->session()->get('companyid');
            $productAttributes->save();
            $index++;
        }


        return redirect('c/products')->with('success','Successfully Add Product');
    }

    function editproduct(Request $request, $id)
    {
        $product = products::where('company_id',$request->session()->get('companyid'))->where('id',$id)->first();
        $categories = Categories::where('company_id',$request->session()->get('companyid'))->get();
        $brandshow = Brands::where('company_id',$request->session()->get('companyid'))->get();
        $subcategories = subcategories::where('company_id',$request->session()->get('companyid'))->get();
        $data["productAttributes"] = Productattributes::where('company_id',$request->session()->get('companyid'))->get();
        return view("c.product.productedit", compact('product','brandshow','categories','subcategories'), $data);
    }

    function editproductpost(Request $request ,$id)
    {


        $request->validate([
            'additionalinformation' => 'max:1000',
            'arabic_additionalinformation' => 'max:1000',
            'videolink' => 'max:70'
        ]);


        // if($request->newarrival)
        // {
        //     $checkNewArrival = products::where('newarrival','on')->get()->count();
        //     if($checkNewArrival >= 15)
        //     {
        //         return back()->with("error","New arrival has limit of 15");
        //     }
        // }
        // else if($request->hotproducts)
        // {
        //     $checkhotproducts = products::where('hotproducts','on')->get()->count();
        //     if($checkhotproducts >= 15)
        //     {
        //         return back()->with("error","Hot products has limit of 15");
        //     }
        // }
        // else if($request->featuredproducts)
        // {
        //     $checkfeaturedproducts = products::where('featuredproducts','on')->get()->count();
        //     if($checkfeaturedproducts >= 15)
        //     {
        //         return back()->with("error","Feature Products has limit of 15");
        //     }
        // }


        $products = products::where('company_id', $request->session()->get('companyid'))->where('id',$id)->first();

        $products->prod_name = $request->name;
        $products->arabic_name = $request->arabic_name;
        $products->description = $request->description;
        $products->arabic_description = $request->arabic_description;
        $products->model = $request->model;
        $products->search_for_related_product_1 = 'showing null';


        if($request->hasfile('product_img')){

            $data = json_decode($products->image);

            foreach($request->file('product_img') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/products/', $name);
                $data[] = $name;
                $products->image = json_encode($data);
            }
        }

        if($request->hasfile('arb_product_img')){

            foreach($request->file('arb_product_img') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/products/', $name);
                $data1[] = $name;
                $products->arabic_image = json_encode($data1);
            }
        }

        $products->videolink = $request->videolink;
        $products->img_height = '310';
        $products->track_stock = $request->track_stock;;
        $products->available_quantity = $request->available_quantity;
        $products->url_keyword = $request->slug;
        $products->title_tag = $request->seo_title;
        $products->meta_tag = $request->meta;
        $products->specification = $request->specification;
        $products->arabic_specification = $request->arabic_specification;

        $products->additionalinformation = $request->additionalinformation;
        $products->arabic_additionalinformation = $request->arabic_additionalinformation;


        $products->newarrival = $request->newarrival;
        $products->hotproducts = $request->hotproducts;
        $products->featuredproducts = $request->featuredproducts;

        $products->shipping = $request->shippable;
        $products->brands = $request->brand;
        $products->sku = $request->sku;
        $products->weight = $request->weight;
        $products->retail = 'retail';
        $products->status = $request->status;
        $products->price = $request->price_1;
        $products->sale_price = $request->saleprice_1;
        $products->all_offers = "";

        $products->categories_id = $request->primary_category;
        $products->sub_category_id = $request->secondary_category;
        $products->created_at = strtotime($request->create_date);

        $products->save();


        $productColor = $request->productcolour;
        $productquantity = $request->quantity;
        $productattributeids = $request->productattributeidd;

        $index = 0;
        $difference = 0;
        foreach($productattributeids as $productattributeid)
        {
            $productAttributes = new Productattributes;

            if(empty($productattributeid))
            {
                $productAttributes->colourname = $productColor[$index];
                $productAttributes->quantity = $productquantity[$index];
                $productAttributes->productid = $id;
                $productAttributes->company_id = $request->session()->get("companyid");
                $productAttributes->save();
                $index++;
            }
            else
            {
                $updateColumns = array(
                    'colourname' => $productColor[$index],
                    'quantity' => $productquantity[$index],
                );

                $productAttributes::where('id', $productattributeid)->update($updateColumns);

                $index++;
            }
        }


        return redirect('c/products');
    }

    function deleteproductattribute(Request $request)
    {


        $productAttribute = new Productattributes;

        $totalproductattribute = $productAttribute::where('productid', $request->productid)->where('company_id', $request->session()->get('companyid'))->count();

        if($totalproductattribute == 1)
        {
            return back()->with('danger','Colour and quantity exist');
        }
        else
        {
            $productAttribute::where('id', $request->productattributeid)->where('productid', $request->productid)->delete();
            return redirect("c/products/edit/".$request->productid);
        }

    }

    function producttrash(Request $request)
    {
        $data["products"] = products::where('status',0)->where('company_id', $request->session()->get('companyid'))->get();
        return view('c.product.products', $data);
    }

    function productdelete(Request $request, $id)
    {
        $products = products::where('company_id', $request->session()->get('companyid'))->where("id", $id)->first();
        $products->delete();
        return redirect('c/products')->with('danger','Product Deleted');
    }




}
