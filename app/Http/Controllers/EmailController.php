<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;


class EmailController extends Controller
{
    //
    public function passwordreset(Request $request)
    {
        $uniqidStr = md5(uniqid(mt_rand()));

        $link = url('')."/setpassword/".$uniqidStr;

        $data = array(
            'name'      =>  $link,
            'message'   =>   'Please click above link to reset password'
        );

        $checkuseremail = User::where('email', $request->email)->first();

        if(gettype($checkuseremail) == "object")
        {
            Mail::to($request->email)->send(new SendEmail($data));
            User::where('email', $request->email)->update(['pass_recov_token' => $uniqidStr, 'password_recovery' => 1]);
            return back()->with('success','Note! An email has been sent to your email address. Please click given link to reset password');
        }
        else
        {
            return back()->with('error','Warning! Invalid Email');
        }
    }

    public function setpassword($token)
    {

        $users = User::where([
            ['pass_recov_token', '=', $token],
            ['password_recovery', '=', 1]])->get();

        if(count($users)>0)
        {
        $data["token"] = $token;
        return view('set_password', $data);
        }
        else
        {
            return redirect('login');
        }
    }

    function set_passwordPost(Request $request)
    {
        $password = $request->password;

        $password = bcrypt($password);

        User::where([
                ['pass_recov_token', '=', $request->token_id],
                ['password_recovery', '=', 1]])
            ->update(['password' => $password, 'password_recovery' => 0]);

        return redirect('login');
    }

    public function sendemail()
    {
        $data = array(
            'message'   =>   'Please click above link to reset password'
        );
        Mail::to('m.atif_khan@yahoo.com')->send(new SendEmail($data));
    }

}
