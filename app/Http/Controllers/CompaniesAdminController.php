<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class CompaniesAdminController extends Controller
{
    //
    public function index()
    {
        $data["customers"] = User::all();
        $data["totalcustomers"] = $data["customers"]->count();
        return view('c.companies', $data);
    }
}
