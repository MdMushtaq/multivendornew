<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Companies;

class NewcompaniesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        //
        $newcompanies = Companies::where('accept',null)->get();
        return view('newcompanies.newcompanies', compact('newcompanies'));
    }

    public function editcompany($id)
    {
        $newcompany = Companies::where('id',$id)->first();
        return view("newcompanies.newcompanyedit", compact('newcompany'));
    }

    public function editnewcompanyformpost (Request $request)
    {
        $company = Companies::find($request->newcompanyid);

        $company->companyname = $request->companyname;
        $company->personname = $request->personname;
        $company->mobile = $request->mobile;
        $company->email = $request->email;
        $company->address = $request->address;
        $company->city = $request->city;
        $company->licensenumber = $request->licensenumber;

        if($request->hasfile('companylogo'))
        {
            $request->validate([
                'companylogo' => 'required|image|mimes:jpeg,png,jpg'
            ]);


            $imagename = request()->companylogo->getClientOriginalName();

            request()->companylogo->move(public_path('images/'), $imagename);

            $company->companylogo = $imagename;
        }

        $company->accounttype = $request->accounttype;
        $company->customernumber = $request->customernumber;

        $company->save();

        return redirect('/admin/newcompanies');
    }

    public function acceptcompany($newcompanyid)
    {
        $newcompany = Companies::find($newcompanyid);
        if($newcompany->request == 1)
        {
            $newcompany->request = null;
        }
        $newcompany->accept = 1;
        $newcompany->save();

        return redirect('/admin/newcompanies');
    }


    public function notacceptcompany($newcompanyid)
    {
        $newcompany = Companies::find($newcompanyid);
        if($newcompany->request == 1)
        {
            $newcompany->request = null;
        }
        $newcompany->accept = 3;
        $newcompany->save();

        return redirect('/admin/newcompanies');
    }

    public function archivecompany($newcompanyid)
    {
        $newcompany = Companies::find($newcompanyid);
        if($newcompany->request == 1)
        {
            $newcompany->request = null;
        }
        $newcompany->accept = 2;
        $newcompany->save();

        return redirect('/admin/newcompanies');
    }

    public function notapproved()
    {
        //
        $newcompanies = Companies::where('accept',3)->get();
        return view('newcompanies.notapproved', compact('newcompanies'));
    }

    public function archives()
    {
        //
        $newcompanies = Companies::where('accept',2)->get();
        return view('newcompanies.archives', compact('newcompanies'));
    }

    public function requests()
    {
        //
        $newcompanies = Companies::where('request',1)->get();
        return view('newcompanies.requests', compact('newcompanies'));
    }

}
