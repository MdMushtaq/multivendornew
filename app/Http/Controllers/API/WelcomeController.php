<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\products;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    public function newarrival()
    {
        $getproducts = products::where(['status' => 1 , 'newarrival' => 'on'])->orderBy('created_at','desc')->limit(15)->get();
        return response()->json([
            'success'=>true, 
            'data'=>$getproducts
        ]);
    }

    public function hotproduct()
    {
        $getproducts = products::where(['status' => 1 , 'hotproducts' => 'on'])->orderBy('created_at','desc')->limit(15)->get();
        return response()->json([
            'success'=>true, 
            'data'=>$getproducts
        ]);
    }
    
}
