<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Companies;

class CompaniesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        //
        $companies = Companies::where('accept',1)->get();
        return view('companies.companies', compact('companies'));
    }

    public function editcompany($id)
    {
        $company = Companies::where('accept',1)->where('id',$id)->first();
        return view("companies.companyedit", compact('company'));
    }

    public function editcompanyformpost(Request $request)
    {
        $company = Companies::find($request->companyid);

        $company->companyname = $request->companyname;
        $company->personname = $request->personname;
        $company->mobile = $request->mobile;
        $company->email = $request->email;
        $company->address = $request->address;
        $company->city = $request->city;
        $company->licensenumber = $request->licensenumber;

        if($request->hasfile('companylogo'))
        {
            $request->validate([
                'companylogo' => 'required|image|mimes:jpeg,png,jpg'
            ]);


            $imagename = request()->companylogo->getClientOriginalName();

            request()->companylogo->move(public_path('images/'), $imagename);

            $company->companylogo = $imagename;
        }

        $company->accounttype = $request->accounttype;
        $company->customernumber = $request->customernumber;

        $company->save();

        return redirect('/admin/companies');
    }

    function companydelete($id)
    {
        $vendor = Companies::find($id);
        $vendor->delete();
        return redirect('admin/companies')->with('danger','Company Deleted');
    }

}
