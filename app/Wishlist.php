<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table = 'wishlist';

    public function products()
    {
        return $this->belongsTo(products::class,'product_id');
    }
}
