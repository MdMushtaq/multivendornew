<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subcategories extends Model
{
    //
    protected $table = 'subcategories';

    public function products()
    {
        return $this->hasMany(products::class, 'sub_category_id');
    }
}
