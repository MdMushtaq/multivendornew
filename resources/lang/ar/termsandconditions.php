<?php

return [
    // termsandconditions.blade.php

    'TechnoSouq Terms and Conditions' => 'الأحكام وشروط الاستخدام لـ(تكنوسوق)',
    'Terms and conditions of use'=>'وثيقة الأحكام وشروط الاستخدام',
    'Dear User of TechnoSouq Applications and Website:'=>'عزيزي/عزيزتي المستخدمـ/ـة لتطبيقات وموقع (تكنوسوق) الإلكتروني:',
    'Using TechnoSouq website and all you rpurchases and useof the products available on this site are subject to a set of terms and conditions. By reading and completing your purchase throughTechnoSouq website and its mobileapplications, you acknowledge and approve all the written terms and conditions.Theyare as follows:'=>'إنّ استخدامك للموقع الإلكتروني لـ(تكنوسوق) وجميع عمليات شرائك واستخدامك للمنتجات المتوفرة في هذا الموقع تخضع لمجموعة من الشروط والأحكام الخاصة بنا.وبقراءتك لها والإطلاع عليها وإتمام عملية شراءك من خلال الموقع الإلكتروني لـ(تكنو سوق) وتطبيقات الجوال التابعة له، فإنّك تُقرّ بها وتوافق عليها. وهي كالتالي:',
    '•	TechnoSouq has the right to accept,decline or cancel your order in certain cases. For example: if the product you wish to purchase is not available, if the product is priced incorrectly or if the order is found to be fraudulent. TechnoSouq will return what you have paid for orders that have not been accepted or cancelled.'=>'•	يحق لـ (تكنوسوق): أن تختار قبول أو عدم قبول أو إلغاء طلبيتك في بعض الحالات و على سبيل المثال إن كان المنتج الذي ترغب بشرائه غير متوفر أو في حال تم تسعير المنتج بطريقة خاطئة.وعليه سوف يقوم موقع (تكنوسوق)بإعادة ماقمت بدفعه للطلبيات التي لم يتم قبولها أو التي يتم إلغاؤها.',
    '•	All content onTechnoSouq  website including: writings, designs, drawings, logos, icons, images, audio clips, downloads, interfaces, icons, codes and software; as well as, how to select and arrange) is an exclusive property owned by TechnoSouq and is subject to copyright and trademark protection.'=>'•	جميع المحتويات على موقع (تكنوسوق): (بما في ذلك على سبيل المثال لا الحصر الكتابات والتصاميم والرسومات والشعارات والأيقونات والصور والمقاطع الصوتية والتحميلات والواجهات والرموز والبرامج بالإضافة إلى كيفية اختيارها وترتيبها) ملكية حصرية يملكها موقع (تكنوسوق) وتكون هذه الملكية خاضعة لحماية حقوق النشر والعلامة التجارية',
    '•	By purchasing or sending an email toTechnoSouq, you agree to receive any emails, notices and alerts from TechnoSouq.'=>'•	عند الشراء أو إرسال بريد الإلكتروني إلى (تكنوسوق): أنت بذلك توافق على استلام أي إيميلات أو إشعارات وتنبيهات من (تكنوسوق).',
    '•	TechnoSouq reserves the right to make any modifications or changes to its website and to the policies and agreements associated with TechnoSouq, including: Privacy Policy, Terms and Conditions Document, Replacement and Return Policy, Shipping and Delivery Policy, and Payment Methods Policy.'=>'•	يحتفظ موقع (تكنوسوق)لنفسه بحق إجراء أيتعديلات أو تغييرات على موقعه وعلى السياسات والإتفاقيات المرتبطة بـ(تكنوسوق) بما في ذلك سياسة الخصوصية وكذلكوثيقة الأحكام وشروط الاستخدام وسياسة الاسترجاع والاستبدال وسياسة الشحن والتوصيل وسياسة طرق الدفعوسياسة التسعير. ',


];

?>