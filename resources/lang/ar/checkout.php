<?php

return [
    // checkout.blade.php
    'Image' => 'صورة',
    'Product' => 'المنتج',
    'Price' => 'السعر',
    'Quantity' => 'كمية',
    'Total' => 'مجموع',
    'SR' => 'ريال سعودى',
    'Continue Shoping' => 'مواصلة التسوق',
    'Cart totals' => 'إجماليات سلة التسوق',
    'Order Subtotal' => 'مجموع النظام الفرعي',
    'VAT' => 'ضريبة القيمة المضافة',
    'Procceed to checkout' => 'انتقل إلى الخروج',
    'There are no items in this cart' => 'لا توجد بنود في هذه العربة',
    'Your Cart' => 'عربتك',
    'Color' => 'اللون',


];

?>