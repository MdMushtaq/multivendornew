<?php

return [
    // privacyandpolicy.blade.php

    'Privacy and Confidentiality' => 'الخصوصية والسرية',
    'What Personal Information About Customers Does Technosouq Collect?'=>'ما هي المعلومات الشخصية عن العملاء التي تجمعها تكنوسوق؟',
    'We collect your personal information in order to provide and continually improve our products and services.'=>'نقوم بجمع معلوماتك الشخصية من أجل توفير منتجاتنا وخدماتنا وتحسينها باستمرار.',
    'Here are the types of personal information we collect:'=>'فيما يلي أنواع المعلومات الشخصية التي نجمعها:',
    'Information You Give Us:'=>'المعلومات التي تقدمها لنا:',
    'Automatic Information:'=>'المعلومات التلقائية:',
    'Information from Other Sources:'=>'معلومات من مصادر أخرى:',
    'We receive and store any information you provide in relation to Technosouq Services. Click here to see examples of what we collect. You can choose not to provide certain information, but then you might not be able to take advantage of many of our Technosouq Services.'=>'نتلقى ونخزن أي معلومات تقدمها فيما يتعلق بخدمات تكنوسوق. انقر هنا لمشاهدة أمثلة على ما نجمعه. يمكنك اختيار عدم تقديم معلومات معينة ، ولكن بعد ذلك قد لا تتمكن من الاستفادة من العديد من خدمات تكنوسوق الخاصة بنا.',
    'We automatically collect and store certain types of information about your use of Technosouq Services, including information about your interaction with content and services available through Technosouq Services. Like many websites, we use "cookies" and other unique identifiers, and we obtain certain types of information when your web browser or device accesses Technosouq Services and other content served by or on behalf of Technosouq on other websites. Click here to see examples of what we collect.'=>'نقوم تلقائيًا بجمع وتخزين أنواع معينة من المعلومات حول استخدامك لخدمات تكنوسوق ، بما في ذلك معلومات حول تفاعلك مع المحتوى والخدمات المتاحة من خلال خدمات تكنوسوق. مثل العديد من مواقع الويب ، نستخدم "ملفات تعريف الارتباط" وغيرها من المعرفات الفريدة ، ونحصل على أنواع معينة من المعلومات عندما يصل متصفح الويب أو الجهاز الخاص بك إلى خدمات تكنوسوق والمحتويات الأخرى التي يتم تقديمها بواسطة أو بالنيابة عن تكنوسوق على مواقع الويب الأخرى. انقر هنا لمشاهدة أمثلة على ما نجمعه.',
    'We might receive information about you from other sources, such as updated delivery and address information from our carriers, which we use to correct our records and deliver your next purchase more easily. Click here to see additional examples of the information we receive.'=>'قد نتلقى معلومات عنك من مصادر أخرى ، مثل معلومات التسليم والعنوان المحدثة من شركات النقل لدينا ، والتي نستخدمها لتصحيح سجلاتنا وتسليم عملية الشراء التالية بسهولة أكبر. انقر هنا لمشاهدة أمثلة إضافية للمعلومات التي نتلقاها.',
    'For What Purposes Does Technosouq Use Your Personal Information?'=>'لأي أغراض تستخدم تكنوسوق معلوماتك الشخصية؟',
    'We use your personal information to operate, provide, develop, and improve the products and services that we offer our customers. These purposes include:'=>'نستخدم معلوماتك الشخصية لتشغيل المنتجات والخدمات التي نقدمها لعملائنا وتوفيرها وتطويرها وتحسينها. تشمل هذه الأغراض:',
    'Purchase and delivery of products and services.'=>'شراء وتسليم المنتجات والخدمات.',
    'Provide, troubleshoot, and improve Technosouq Services.'=>'تقديم واستكشاف الأخطاء وإصلاحها وتحسين خدمات تكنوسوق.',
    'Recommendations and personalization.'=>'التوصيات والتخصيص.',
    'Provide voice, image and camera services.'=>'تقديم خدمات الصوت والصورة والكاميرا.',
    'Comply with legal obligations.'=>'الامتثال للالتزامات القانونية.',
    'We use your personal information to take and handle orders, deliver products and services, process payments, and communicate with you about orders, products and services, and promotional offers.'=>'نستخدم معلوماتك الشخصية لتلقي الطلبات ومعالجتها ، وتقديم المنتجات والخدمات ، ومعالجة المدفوعات ، والتواصل معك بشأن الطلبات والمنتجات والخدمات والعروض الترويجية.',
    'We use your personal information to provide functionality, analyze performance, fix errors, and improve the usability and effectiveness of the Technosouq Services'=>'نستخدم معلوماتك الشخصية لتوفير الوظائف ، وتحليل الأداء ، وإصلاح الأخطاء ، وتحسين إمكانية استخدام وفعالية خدمات تكنوسوق.',
    'We use your personal information to provide functionality, analyze performance, fix errors, and improve the usability and effectiveness of the Technosouq Services.'=>'نستخدم معلوماتك الشخصية لتوفير الوظائف ، وتحليل الأداء ، وإصلاح الأخطاء ، وتحسين قابلية استخدام وفعالية خدمات تكنوسوق.',
    'We use your personal information to recommend features, products, and services that might be of interest to you, identify your preferences, and personalize your experience with Technosouq Services.'=>'نستخدم معلوماتك الشخصية للتوصية بالميزات والمنتجات والخدمات التي قد تهمك ، وتحديد تفضيلاتك ، وإضفاء الطابع الشخصي على تجربتك مع خدمات تكنوسوق.',
    'When you use our voice, image and camera services, we use your voice input, images, videos, and other personal information to respond to your requests, provide the requested service to you, and improve our services. For more information about Alexa voice services .'=>'عندما تستخدم خدمات الصوت والصورة والكاميرا الخاصة بنا ، فإننا نستخدم الإدخال الصوتي والصور ومقاطع الفيديو والمعلومات الشخصية الأخرى للرد على طلباتك وتقديم الخدمة المطلوبة لك وتحسين خدماتنا. لمزيد من المعلومات حول خدمات Alexa الصوتية.',
    'In certain cases, we collect and use your personal information to comply with laws. For instance, we collect from sellers information regarding place of establishment and bank account information for identity verification and other purposes.'=>'في حالات معينة ، نجمع معلوماتك الشخصية ونستخدمها للامتثال للقوانين. على سبيل المثال ، نقوم بجمع معلومات من البائعين بخصوص مكان التأسيس ومعلومات الحساب المصرفي للتحقق من الهوية ولأغراض أخرى.',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
];

?>