<?php
   return [
      // welcome.blade.php
 
      'View All' => 'مشاهدة الكل',
      'New Products' => 'منتجات جديدة',
      'Popular Products' => 'المنتجات الشعبية',
      'Top Selling Products' => 'المنتجات الأكثر مبيعًا',
      'Filter Product' => 'مرشح المنتج',
      'SAR' => 'ريال',
      'ADD TO CART' => 'أضف إلى السلة',
      'CART' => 'عربة التسوق',


      // layouts front-app.blade.php
      'FREE AND ESAY RETURNS' => 'ترجيع مجانا وبسهولة',
      'BEST DEALS' => 'افضل العروض',
      'TOP BRANDS' => 'ارقى الماركات',
      'SIGN IN' => 'تسجيل الدخول',
      'SIGN UP' => 'سجل',
      'Select Language' => 'اختار اللغة',

      // Header menu Bar
      'CATEEGORIES' => 'الفئات',
      'Electronics' => 'إلكترونيات',
      'PRODUCTS' => 'منتجات',
      'SPECIAL OFFER' => 'عرض خاص',
      'Offer' => 'عرض',
      'ABOUT US' => 'معلومات عنا',
      'HELP' => 'مساعدة',
      'DEALS' => 'صفقات',
      'CONTACT US' => 'اتصل بنا',
      'Contact Us' => 'اتصل بنا',
      'Complaint' => 'شكوى',
      'SHOP MENU' => 'قائمة التسوق',
      'DELIVER TO' => 'يسلم إلى',
      'SAUDIA ARABIA' => 'المملكة العربية السعودية',
      'FLASH' => 'فلاش',
      'SALE' => 'تخفيض السعر',
      'END IN' => 'ينتهي في',

       //Footer
       'About Us' => 'معلومات عنا',
       'Careers' => 'وظائف',
       'Media' => 'وسائل الإعلام',
       'Initiatives' => 'المبادرات',
       'FAQ' => 'التعليمات',
       'Terms & Conditions' => 'البنود و الظروف',
       'Privacy and Policy' => 'الخصوصية والسياسة',
       'Phone' => 'هاتف',
       'Email' => 'البريد الإلكتروني',
       'Working Days Hours' => 'ساعات أيام العمل',
       'Let Us Help You' => 'دعنا نساعدك',
       'Your Account' => 'الحساب الخاص بك',
       'Your Orders' => 'طلباتك',
       'Shipping Rates & Policies' => 'أسعار الشحن والسياسات',
       'Payment Method' => 'طريقة الدفع او السداد',
       'All Rights Reserved' => 'كل الحقوق محفوظة',
       'Powered by' => 'مشغل بواسطة',
       'Contact Information' => 'معلومات الاتصال',
       'Quick links' => 'روابط سريعة',
       'NEW ARRIVAL' => 'قادم جديد',
       'View More' => 'عرض المزيد',
       'SR' => 'ريال',
       'HOT PRODUCTS' => 'منتوجات جديدة',
       'FEATURE CATEEGORIES' => 'فئات مميزة',
       'Get The Latest News' => 'احصل على آخر الأخبار',
       'Enter your Email' => 'أدخل بريدك الإلكتروني',
       'Subscribe' => 'الإشتراك',


   ];
?>