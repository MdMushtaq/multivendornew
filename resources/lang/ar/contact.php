<?php

return [
    // contact.blade.php

    'CONTACT US' => 'تواصل معنا',
    'Feel free to reach us at anytime with your inquires, concerns or feedback.' => 'لا تتردد في التواصل معنا في أي وقت مع استفساراتك أو مخاوفك أو ملاحظاتك.',
    'Telephone: +966-12-6694622' => 'رقم الهاتف: 6694622-12-966 +',
    'Fax: +966-12-6673075' => 'الفاكس: + 966-12-6673075',
    'P.O Box: 1338, Jeddah 21431 Saudi Arabia' => 'صندوق بريد: 1338 جدة 21431 المملكة العربية السعودية',
    'Get In Touch With Us' => 'ابق على تواصل معنا',
    'Name' => 'اسم',
    'Email' => 'البريد الإلكتروني',
    'Mobile Contact Number' => 'رقم الهاتف المحمول',
    'Order Number If you have an order' => 'رقم الطلب إذا كان لديك طلب',
    'Enquiry' => 'تحقيق',
    'Save' => 'حفظ',
];

?>