<?php

return [
    // productlist.blade.php

    'PRODUCT CATEEGORIES' => 'فئات المنتج',
    'Home' => 'الصفحة الرئيسية',
    'SR:' => 'ريال:',
    'Vat' => 'ضريبة القيمة المضافة',
    'Total Price' => 'السعر الكلي',
    'ADD TO CART' => 'أضف إلى السلة',
    'Colors:' => 'الألوان:',
    'SPECIFICATION' => 'تخصيص',
    'Description' => 'وصف',
    'Additional Information' => 'معلومة اضافية',
    'Weight' => 'وزن',
    'Dimensions' => 'الأبعاد',
    'kg' => 'كلغ',
    'cm' => 'سم',
    'Recommended' => 'موصى به',
    'View More' => 'عرض المزيد',
    'Successfully Added to Your Cart' => 'تمت الإضافة بنجاح إلى عربة التسوق الخاصة بك',
    'Color'=>'اللون',
];

?>