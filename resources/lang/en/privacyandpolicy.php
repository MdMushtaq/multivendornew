<?php

return [
    // privacyandpolicy.blade.php

    'Privacy and Confidentiality' => 'Privacy and Confidentiality',
    'What Personal Information About Customers Does Technosouq Collect?'=>'What Personal Information About Customers Does Technosouq Collect?',
    'We collect your personal information in order to provide and continually improve our products and services.'=>'We collect your personal information in order to provide and continually improve our products and services.',
    'Here are the types of personal information we collect:'=>'Here are the types of personal information we collect:',
    'Information You Give Us:'=>'Information You Give Us:',
    'Automatic Information:'=>'Automatic Information:',
    'Information from Other Sources:'=>'Information from Other Sources:',
    'We receive and store any information you provide in relation to Technosouq Services. Click here to see examples of what we collect. You can choose not to provide certain information, but then you might not be able to take advantage of many of our Technosouq Services.'=>'We receive and store any information you provide in relation to Technosouq Services. Click here to see examples of what we collect. You can choose not to provide certain information, but then you might not be able to take advantage of many of our Technosouq Services.',
    'We automatically collect and store certain types of information about your use of Technosouq Services, including information about your interaction with content and services available through Technosouq Services. Like many websites, we use "cookies" and other unique identifiers, and we obtain certain types of information when your web browser or device accesses Technosouq Services and other content served by or on behalf of Technosouq on other websites. Click here to see examples of what we collect.'=>'We automatically collect and store certain types of information about your use of Technosouq Services, including information about your interaction with content and services available through Technosouq Services. Like many websites, we use "cookies" and other unique identifiers, and we obtain certain types of information when your web browser or device accesses Technosouq Services and other content served by or on behalf of Technosouq on other websites. Click here to see examples of what we collect.',
    'We might receive information about you from other sources, such as updated delivery and address information from our carriers, which we use to correct our records and deliver your next purchase more easily. Click here to see additional examples of the information we receive.'=>'We might receive information about you from other sources, such as updated delivery and address information from our carriers, which we use to correct our records and deliver your next purchase more easily. Click here to see additional examples of the information we receive.',
    'For What Purposes Does Technosouq Use Your Personal Information?'=>'For What Purposes Does Technosouq Use Your Personal Information?',
    'We use your personal information to operate, provide, develop, and improve the products and services that we offer our customers. These purposes include:'=>'We use your personal information to operate, provide, develop, and improve the products and services that we offer our customers. These purposes include:',
    'Purchase and delivery of products and services.'=>'Purchase and delivery of products and services.',
    'Provide, troubleshoot, and improve Technosouq Services.'=>'Provide, troubleshoot, and improve Technosouq Services.',
    'Recommendations and personalization.'=>'Recommendations and personalization.',
    'Provide voice, image and camera services.'=>'Provide voice, image and camera services. ',
    'Comply with legal obligations.'=>'Comply with legal obligations.',
    'We use your personal information to take and handle orders, deliver products and services, process payments, and communicate with you about orders, products and services, and promotional offers.'=>'We use your personal information to take and handle orders, deliver products and services, process payments, and communicate with you about orders, products and services, and promotional offers.',
    'We use your personal information to provide functionality, analyze performance, fix errors, and improve the usability and effectiveness of the Technosouq Services'=>'We use your personal information to provide functionality, analyze performance, fix errors, and improve the usability and effectiveness of the Technosouq Services',
    'We use your personal information to provide functionality, analyze performance, fix errors, and improve the usability and effectiveness of the Technosouq Services.'=>'We use your personal information to provide functionality, analyze performance, fix errors, and improve the usability and effectiveness of the Technosouq Services.',
    'We use your personal information to recommend features, products, and services that might be of interest to you, identify your preferences, and personalize your experience with Technosouq Services.'=>'We use your personal information to recommend features, products, and services that might be of interest to you, identify your preferences, and personalize your experience with Technosouq Services.',
    'When you use our voice, image and camera services, we use your voice input, images, videos, and other personal information to respond to your requests, provide the requested service to you, and improve our services. For more information about Alexa voice services .'=>'When you use our voice, image and camera services, we use your voice input, images, videos, and other personal information to respond to your requests, provide the requested service to you, and improve our services. For more information about Alexa voice services .',
    'In certain cases, we collect and use your personal information to comply with laws. For instance, we collect from sellers information regarding place of establishment and bank account information for identity verification and other purposes.'=>'In certain cases, we collect and use your personal information to comply with laws. For instance, we collect from sellers information regarding place of establishment and bank account information for identity verification and other purposes.',


];

?>