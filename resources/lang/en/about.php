<?php

return [
    // about.blade.php

    'About Us' => 'About Us',
    'Who we are' => 'Who we are',
    'Technosouq is guided by four principles: customer obsession rather than competitor focus, passion for invention, commitment to operational excellence, and long-term thinking' => 'Technosouq is guided by four principles: customer obsession rather than competitor focus, passion for invention, commitment to operational excellence, and long-term thinking.',
    'Bulding the future' => 'Bulding the future',
    'We strive to have a positive impact on customers, employees, small businesses, the economy, and communities. Amazonians are smart, passionate builders with different backgrounds and goals, who share a common desire to always be learning and inventing on behalf of our customers.' => 'We strive to have a positive impact on customers, employees, small businesses, the economy, and communities. Amazonians are smart, passionate builders with different backgrounds and goals, who share a common desire to always be learning and inventing on behalf of our customers.',
    'What we do' => 'What we do',
    'Explore Technosouq’s products and services.' => 'Explore Technosouq’s products and services.',
    'Technosouq' => 'Technosouq'

];

?>