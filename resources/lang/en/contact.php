<?php

return [
    // contact.blade.php

    'CONTACT US' => 'CONTACT US',
    'Feel free to reach us at anytime with your inquires, concerns or feedback.' => 'Feel free to reach us at anytime with your inquires, concerns or feedback.',
    'Telephone: +966-12-6694622' => 'Telephone: +966-12-6694622',
    'Fax: +966-12-6673075' => 'Fax: +966-12-6673075',
    'P.O Box: 1338, Jeddah 21431 Saudi Arabia' => 'P.O Box: 1338, Jeddah 21431 Saudi Arabia',
    'Get In Touch With Us' => 'Get In Touch With Us',
    'Name' => 'Name',
    'Email' => 'Email',
    'Mobile Contact Number' => 'Mobile Contact Number',
    'Order Number If you have an order' => 'Order Number If you have an order',
    'Enquiry' => 'Enquiry',
    'Save' => 'Save',
];

?>