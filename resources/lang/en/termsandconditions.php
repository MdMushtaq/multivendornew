<?php

return [
    // termsandconditions.blade.php

    'TechnoSouq Terms and Conditions' => 'TechnoSouq Terms and Conditions',
    'Terms and conditions of use'=>'Terms and conditions of use',
    'Dear User of TechnoSouq Applications and Website:'=>'Dear User of TechnoSouq Applications and Website:',
    'Using TechnoSouq website and all you rpurchases and useof the products available on this site are subject to a set of terms and conditions. By reading and completing your purchase throughTechnoSouq website and its mobileapplications, you acknowledge and approve all the written terms and conditions.Theyare as follows:'=>'Using TechnoSouq website and all you rpurchases and useof the products available on this site are subject to a set of terms and conditions. By reading and completing your purchase throughTechnoSouq website and its mobileapplications, you acknowledge and approve all the written terms and conditions.Theyare as follows:',
    '•	TechnoSouq has the right to accept,decline or cancel your order in certain cases. For example: if the product you wish to purchase is not available, if the product is priced incorrectly or if the order is found to be fraudulent. TechnoSouq will return what you have paid for orders that have not been accepted or cancelled.'=>'•	TechnoSouq has the right to accept,decline or cancel your order in certain cases. For example: if the product you wish to purchase is not available, if the product is priced incorrectly or if the order is found to be fraudulent. TechnoSouq will return what you have paid for orders that have not been accepted or cancelled.',
    '•	All content onTechnoSouq  website including: writings, designs, drawings, logos, icons, images, audio clips, downloads, interfaces, icons, codes and software; as well as, how to select and arrange) is an exclusive property owned by TechnoSouq and is subject to copyright and trademark protection.'=>'•	All content onTechnoSouq  website including: writings, designs, drawings, logos, icons, images, audio clips, downloads, interfaces, icons, codes and software; as well as, how to select and arrange) is an exclusive property owned by TechnoSouq and is subject to copyright and trademark protection.',
    '•	By purchasing or sending an email toTechnoSouq, you agree to receive any emails, notices and alerts from TechnoSouq.'=>'•	By purchasing or sending an email toTechnoSouq, you agree to receive any emails, notices and alerts from TechnoSouq.',
    '•	TechnoSouq reserves the right to make any modifications or changes to its website and to the policies and agreements associated with TechnoSouq, including: Privacy Policy, Terms and Conditions Document, Replacement and Return Policy, Shipping and Delivery Policy, and Payment Methods Policy.'=>'•	TechnoSouq reserves the right to make any modifications or changes to its website and to the policies and agreements associated with TechnoSouq, including: Privacy Policy, Terms and Conditions Document, Replacement and Return Policy, Shipping and Delivery Policy, and Payment Methods Policy.',





];

?>