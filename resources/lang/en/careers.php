<?php

return [
    // careers.blade.php

    'Job Portal' => 'Job Portal',
    'Achieving success by respecting and empowering personal potential.' => 'Achieving success by respecting and empowering personal potential.',
    'Our passion and ingenuity fuels inspired design. We are always looking for talented, dedicated and passionate teammates who want to carve their own path exploring technological innovation in engineering. Visit our careers page to find out more!' => 'Our passion and ingenuity fuels inspired design. We are always looking for talented, dedicated and passionate teammates who want to carve their own path exploring technological innovation in engineering. Visit our careers page to find out more!',
    'There is no job available right now. Kindly upload your CV we will contact you asap.' => 'There is no job available right now. Kindly upload your CV we will contact you asap.',
    'Register' => 'Register',
    'Back' => 'Back',
    'Your Name' => 'Your Name',
    'Your Email' => 'Your Email',
    'Phone Number' => 'Phone Number',
    'Job Title' => 'Job Title',
];

?>