<?php

return [
    // checkout.blade.php
    'Image' => 'Image',
    'Product' => 'Product',
    'Price' => 'Price',
    'Quantity' => 'Quantity',
    'Total' => 'Total',
    'SR' => 'SR',
    'Continue Shoping' => 'Continue Shoping',
    'Cart totals' => 'Cart totals',
    'Order Subtotal' => 'Order Subtotal',
    'VAT' => 'VAT',
    'Procceed to checkout' => 'Procceed to checkout',
    'Continue Shoping' => 'Continue Shoping',
    'There are no items in this cart' => 'There are no items in this cart',
    'Your Cart' => 'Your Cart',
    'Color' => 'Color',

];

?>