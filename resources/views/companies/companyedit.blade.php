@extends('layouts.admin-app')
@section('content')

<div class="container">

    <!--          <h1>Good</h1>-->
    <!--          <h1>Yes</h1>-->


    <div class="page-header"><h1>Company Form Edit</h1></div>
     
    <form action="{{ url('/admin/companies/formeditpost') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <input type="hidden" name="companyid" value="<?= $company->id; ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Company Name
                            </label>
                            <input type="text" name="companyname" value="{{ $company->companyname }}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Person Name                    </label>
                            <input type="text" name="personname" value="{{ $company->personname }}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
           
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mobile</label>
                            <input type="text" name="mobile" value="{{ $company->mobile }}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="{{ $company->email }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="address" value="{{ $company->address }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>City</label>
                            <input type="text" name="city" value="{{ $company->city }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Account Type</label>
                            <input type="text" name="accounttype" value="{{ $company->accounttype }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Customer Number</label>
                            <input type="text" name="customernumber" value="{{ $company->customernumber }}" class="form-control">
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>License Number</label>
                            <input type="text" name="licensenumber" value="{{ $company->licensenumber }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Company Logo</label>
                            <input type="file" name="companylogo" value="{{ $company->licensenumber }}" class="form-control">
                            <br>
                            <img src="{{ url('/public/images',$company->companylogo) }}" width="100px" height="100px" alt="" data-toggle="modal" data-target="#myModal">
                        </div>
                    </div>

                    <!-- Trigger the modal with a button -->
<!--                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->

                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-body">
                                    <center>
                                        <img style="max-width: 400px;" src="{{ url('/public/images',$company->licenseimage) }}" alt="" data-toggle="modal" data-target="#myModal">
                                    </center>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>


            </div>
      
        </div>
        <div class="row">
            <div class="col-md-10">
                <button type="submit" class="btn btn-primary">
                    Save        </button>
            </div>
            <div class="col-md-2">
            </div>
        </div>


    </form>
    <script type="text/javascript">
        $('form').submit(function() {
            $('.btn .btn-primary').attr('disabled', true).addClass('disabled');
        });
    </script>    <hr>
    <footer></footer>
</div>

@endsection
