@extends('layouts.company-admin-app')
@section('content')
@if(count($errors))
<div class="alert alert-danger">
   <strong>Whoops!</strong> There were some problems with your input.
   <br/>
   <ul>
	   @foreach($errors->all() as $error)
		   <li>{{ $error }}</li>
	   @endforeach
   </ul>
</div>
@endif 


<div class="page-header">
	<h1>Add New Brand</h1>
</div>
<form action="{{route('brands.company.create')}}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	@csrf
	<div class="row">
    	<div class="col-md-12">
        	<div class="row">
            	<div class="col-md-6">
               		<div class="form-group">
                		<label for="name">Name</label>
                		<input type="text" name="name" value="" class="form-control">
              		</div>
            	</div>
	            <div class="col-md-6">
	            	<div class="form-group">
	               		<label for="name">Arabic Name</label>
	               		<input type="text" name="arabic_name" value="" class="form-control arabic-input" lang="ar" dir="rtl">
	            	</div>
	            </div>
        	</div>
			<div class="row">
			    <div class="col-md-6">
			    	<div class="form-group">
			            <label for="image">Image </label>
			            <div class="input-append">
			                <input type="file" name="image" class="form-control">
			            </div>
			    	</div>
			    </div>
			
			    <div class="col-md-6">
			        <label>Status</label>
			        <div class="form-group">
			        	<select name="status" class="form-control">
							<option value="1">Enabled</option>
							<option value="0">Disabled</option>
						</select>
			        </div>
	            </div>
    		</div>
        	<br>
        	<fieldset>
		        <legend>SEO INFORMATION</legend>     
		        <div class="row">
		            <div class="col-md-6">
		            	<div class="form-group">
		            		<label for="seo_title">SEO Title </label>
		            		<input type="text" name="seo_title" value="" class="form-control">
		              	</div>        
		         	</div>
		          	<div class="col-md-6">
		            	<div class="form-group">
		            		<label for="slug">Slug </label>
		            		<input type="text" name="slug" value="" class="form-control">
		             	</div>
		        	</div>        
		    	</div>
		    	<div class="row">
			        <div class="col-md-6">
			             <div class="form-group">
			            	<label for="sequence">Sequence </label>
			            	<input type="text" name="sequence" value="" class="form-control">
			        	</div>
			        </div>
			        <div class="col-md-6">
			            <div class="form-group">
			            	<label>Meta Data</label> 
			            	<textarea name="meta" cols="40" rows="3" class="form-control"></textarea>
			            	<span class="help-block">ex. &lt;meta name="description" content="We sell products that help you" /&gt;</span>
			        	</div>
			     	</div>
				</div>
			</fieldset>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-10">
	        <button type="submit" name="submit" class="btn btn-primary">Save</button>
	    </div>
	    <div class="col-md-2"></div>
	</div>
</form>


@endsection