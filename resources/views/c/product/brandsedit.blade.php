@extends('layouts.company-admin-app')
@section('content')


<div class="page-header">
	<h1>Add New Brand</h1>
</div>
<form action="{{route('brnads.company.update',$brands->id)}}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	@csrf
	<div class="row">
    	<div class="col-md-12">
        	<div class="row">
            	<div class="col-md-6">
               		<div class="form-group">
                		<label for="name">Name</label>
					   <input type="text" name="name" value="{{$brands->brands_name}}" class="form-control">
              		</div>
            	</div>
	            <div class="col-md-6">
	            	<div class="form-group">
	               		<label for="name">Arabic Name</label>
	               		<input type="text" name="arabic_name" value="{{$brands->brands_arb_name}}" class="form-control arabic-input" lang="ar" dir="rtl">
	            	</div>
	            </div>
        	</div>
			<div class="row">
			    <div class="col-md-6">
			    	<div class="form-group">
			            <label for="image">Image </label>
			            <div class="input-append">
							<input type="file" name="image" class="form-control">
							
							<img src="{{asset('public/images/brands/'.$brands->brands_img)}}" alt="">
			            </div>
			    	</div>
			    </div>
			
			    <div class="col-md-6">
			        <label>Status</label>
			        <div class="form-group">
			        	<select name="status" class="form-control">
							<option value="1" {{$brands->brands_status == 1 ? 'Selected' : ''}}>Enabled</option>
							<option value="0" {{$brands->brands_status == 0 ? 'Selected' : ''}}>Disabled</option>
						</select>
			        </div>
	            </div>
    		</div>
        	<br>
        	<fieldset>
		        <legend>SEO INFORMATION</legend>     
		        <div class="row">
		            <div class="col-md-6">
		            	<div class="form-group">
		            		<label for="seo_title">SEO Title </label>
		            		<input type="text" name="seo_title" value="{{$brands->brands_seo_title}}" class="form-control">
		              	</div>        
		         	</div>
		          	<div class="col-md-6">
		            	<div class="form-group">
		            		<label for="slug">Slug </label>
		            		<input type="text" name="slug" value="{{$brands->brands_slug}}" class="form-control">
		             	</div>
		        	</div>        
		    	</div>
		    	<div class="row">
			        <div class="col-md-6">
			             <div class="form-group">
			            	<label for="sequence">Sequence </label>
			            	<input type="text" name="sequence" value="{{$brands->brands_sequence}}" class="form-control">
			        	</div>
			        </div>
			        <div class="col-md-6">
			            <div class="form-group">
			            	<label>Meta Data</label> 
			            	<textarea name="meta" cols="40" rows="3" class="form-control">{{$brands->brands_meta_data}}</textarea>
			            	<span class="help-block">ex. &lt;meta name="description" content="We sell products that help you" /&gt;</span>
			        	</div>
			     	</div>
				</div>
			</fieldset>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-10">
	        <button type="submit" name="submit" class="btn btn-primary">Save</button>
	    </div>
	    <div class="col-md-2"></div>
	</div>
</form>


@endsection