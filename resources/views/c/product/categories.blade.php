@extends('layouts.company-admin-app')
@section('content')


    @if (\Session::has('success'))
    <div class="alert alert-success">
    <p>{{ \Session::get('success') }}</p>
    </div>
    <br />

    @elseif(\Session::has('danger'))
    <div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
    </div>
    <br />
    @endif 

        <div class="page-header"><h1>Categories</h1></div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        &nbsp;
                    </div>
                    <div class="col-md-8">
                        <form action="" class="form-inline form-group" style="float:right" method="post" accept-charset="utf-8">
                            @csrf
                            <div class="form-group">
                                <input type="text" id="myInput" onkeyup="myFunction()" class="form-control" name="catname" value="" placeholder="">
                            </div>
<!--                            <button class="btn btn-default" type="submit" name="submit" value="search">Search</button>-->
                            <a class="btn btn-default" href="#">Reset</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      
        <div style="text-align:right">
            <a class="btn btn-primary" href="{{ url("c/categories/form") }}"><i class="icon-plus"></i> Add New Category</a>
            <a class="btn btn-danger" href="{{ route('trash.company.categories') }}"><i class="icon-reset"></i> Trash Category</a>
        </div>

        <table class="table table-striped" id="myTable">
            <thead>
            <tr>
                <!--<th><i class="icon-eye-slash"></i></th>-->
                <th>Category name</th>
                <th>Catgory Arabic Name </th>
                <th>Status</th>
                    <th>Action</th>

            </tr>
            </thead>
            <tbody>

            @foreach ($categories as $category)
                 <tr>
                <!--<td></td>-->
                <td>{{$category->cat_name}}</td>
                <td>{{$category->cat_arabic_name}}</td>
                <td>{{ $category->cat_status == '0' ? 'Disable' : 'Enable'}}</td>

                <td >
                    <div class="btn-group">

                    <a class="btn btn-default" href="{{route('edit.company.categories',$category->id)}}"><i class="icon-pencil"></i></a>

                        @if( $category->cat_status == '0')

                        <a class="btn btn-danger" href="{{route('delete.company.categories',$category->id)}}" onclick="return confirm('are you sure you wan to delete?')"><i class="icon-times"></i></a>

                        @endif
                </div>
                </td>
            </tr>
            @endforeach


            </tbody>
        </table>
<script>
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

</script>
@endsection
