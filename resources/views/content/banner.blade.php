@extends('layouts.admin-app')
@section('content')
@if(\Session::has('danger'))
<div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
</div>
@endif
<div class="page-header">
	<h1>Banner Collections</h1>
</div>
<a class="btn btn-primary pull-right" href="{{ url('admin/banners/add') }}"><i class="icon-plus"></i> Add New Banner Collection</a>
<table class="table table-striped">
    <thead>
        <tr>
            <th>English Image</th>
            <th>Arabic Image</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($banners as $banner)
    <tr>
        <td>
            <img src="{{ url('/public/images/banners',$banner->banner_img) }}" width="30px" height="30px" alt="">
        </td>
        <td>
            <img src="{{ url('/public/images/banners',$banner->banner_arabic_img) }}" width="30px" height="30px" alt="">
        </td>
        <td class="text-right">
            <div class="btn-group">
                <a class="btn btn-default" href="{{ url('admin/banners/edit/') }}<?= '/'.$banner->id; ?>">
                    <i class="icon-pencil"></i>
                </a>
                <a class="btn btn-danger" href="{{ url('admin/banners/delete', $banner->id) }}">
                    <i class="icon-close"></i>
                </a>
            </div>
        </td>
    </tr>
    @endforeach


    </tbody>
</table>
@endsection