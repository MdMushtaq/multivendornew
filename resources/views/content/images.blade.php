@extends('layouts.admin-app')
@section('content')
@if(\Session::has('danger'))
<div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
</div>
@endif
<div class="page-header">
    <h1>Icon Images</h1>
</div>
<a class="btn btn-primary pull-right" href="{{ url('admin/images/add') }}"><i class="icon-plus"></i> Add New Icon Images</a>
<table class="table table-striped">
    <thead>
    <tr>
        <th>English Icon Image</th>
        <th>Arabic Icon Image</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($iconimages as $iconimage)
    <tr>
        <td>
            <img src="{{ url('/public/images/iconimages',$iconimage->icon_img) }}" width="30px" height="30px" alt="">
        </td>
        <td>
            <img src="{{ url('/public/images/iconimages',$iconimage->icon_arabic_img) }}" width="30px" height="30px" alt="">
        </td>
        <td class="text-right">
            <div class="btn-group">
                <a class="btn btn-default" href="{{ url('admin/images/edit/') }}<?= '/'.$iconimage->id; ?>">
                    <i class="icon-pencil"></i>
                </a>
                <a class="btn btn-danger" href="{{ url('admin/images/delete', $iconimage->id) }}">
                    <i class="icon-close"></i>
                </a>
            </div>
        </td>
    </tr>
    @endforeach


    </tbody>
</table>
@endsection