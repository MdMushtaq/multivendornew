@extends('layouts.admin-app')
@section('content')
<div class="page-header">
    <h1>Edit Icon Images</h1>
</div>
@if(count($errors))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <br/>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form action="{{ url('admin/images/editpost') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
    {{ csrf_field() }}
    <input type="hidden" name="bannerid" value="<?= $iconimage->id; ?>">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="image">English Icon Image</label>
                <div class="input-append">
                    <input type="file" name="icon_image" multiple class="form-control">
                    <br>
                    <img src="{{ asset('public/images/iconimages/'.$iconimage->icon_img) }}" style="margin-bottom: 10px;margin-right: 5px;" width="100" height="100" alt="">
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="image">Arabic Icon Image </label>
                <div class="input-append">
                    <input type="file" name="icon_arabic_image" multiple class="form-control">
                    <br>
                    <img src="{{asset('public/images/banners/'.$iconimage->icon_arabic_img)}}" style="margin-bottom: 10px;margin-right: 5px;" width="100" height="100" alt="">
                </div>

            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-2">
            <input class="btn btn-primary" type="submit" value="Save">
        </div>
        <div class="col-md-10"></div>
    </div>
</form>
@endsection