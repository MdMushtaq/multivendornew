@extends('layouts.admin-app')
@section('content')

@if(count($errors))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <br/>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">

<!--          <h1>Good</h1>-->
<!--          <h1>Yes</h1>-->



<div class="page-header">
    <h1>Customers</h1>
</div>

{{-- <input type="button" onclick="tableToExcel('testTable', 'W3C Example Table')" value="Export to Excel"> --}}

{{-- <div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <nav><ul class="pagination"><li class="active"><a href="#">1</a></li><li></li><li><a href="https://handartis.com/admin/customers/50/customer_id/ASC/0/50" data-ci-pagination-page="50">2</a></li><li></li><li><a href="https://handartis.com/admin/customers/50/customer_id/ASC/0/50" data-ci-pagination-page="50">»</a></li></ul></nav>  &nbsp;
            </div>
            <div class="col-md-8">
                <form action="https://handartis.com/admin/customers" class="form-inline form-group" style="float:right" method="post" accept-charset="utf-8">

                    <div class="form-group">
                        <select name="lang" id="lang" class="form-control">
                            <option value="">--Search by--</option>
                            <option value="first_name">First Name</option>
                            <option value="last_name">Last Name</option>
                            <option value="email">Email</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="term" value="" placeholder="">
                    </div>
                    <button class="btn btn-default" name="submit" value="search">Search</button>
                    <a class="btn btn-default" href="#">Reset</a>
                </form>
            </div>
        </div>
    </div>
</div> --}}
<div class="btn-group pull-right">
    <br>
    <!--  <a class="btn btn-primary" href="https://handartis.com/admin/customers/export"><i class="icon-download"></i> Export Customers (JSON)</a> -->
    <a class="btn btn-primary" href="{{ url('/admin/customers/form') }}"><i class="icon-plus"></i> Add New Customer</a>
    <a class="btn btn-primary" style="margin-left: 5px;" href="{{ url('/admin/customers/archive') }}"> Archive Customer</a>
    <a class="btn btn-primary" style="margin-left: 5px;" href="{{ url('/admin/customers/suspend') }}"> Suspend Customer</a>
    <br>
</div>

<table id="testTable" class="table table-striped">
<!-- <thead>
    <tr>
        <th><a href="https://handartis.com/admin/customers/lastname/ASC/0">Last Name</a></th><th><a href="https://handartis.com/admin/customers/firstname/ASC/0">First Name</a></th><th><a href="https://handartis.com/admin/customers/email/ASC/0">Email</a></th><th><a href="https://handartis.com/admin/customers/active/ASC/0">Active</a></th>            <th></th>
    </tr>
</thead> -->
<thead>
<tr>
    <th>S. No.</th>
    <th>Joining Date</th>
    {{-- <th>Customer Id</th> --}}
    <th>Name</th>
    {{-- <th>Last Name</th> --}}
    <th>Mobile Number</th>
    <th>Email </th>
    <th>Gender</th>
    <th>Country</th>
    <th>City</th>
</tr>
</thead>

<tbody>

<?php
$count = 1;
foreach($customers as $customer)
{
    ?>
    <tr>
        <td><?= $count; ?></td>
        <td><?= $customer->created_at; ?></td>
       
        <td class="gc_cell_left"><?= $customer->name; ?></td>
        
        <td class="gc_cell_left"><?= $customer->phone; ?></td>
        <td class="gc_cell_left"><?= $customer->email; ?></td>
        <td class="gc_cell_left"><?= $customer->gender; ?></td>
        <td class="gc_cell_left"><?= $customer->country; ?></td>
        <td class="gc_cell_left"><?= $customer->city; ?></td>
        <td class="text-right">
            <div class="btn-group">
                <a class="btn btn-default" title="Edit Customer" href="{{ url('admin/customers/form/') }}<?= '/'.$customer->id; ?>"><i class="icon-pencil"></i></a>
                {{-- <a class="btn btn-default" title="View Customer Address" href="/admin/customers/addresses/72974"><i class="icon-envelope"></i></a> --}}
            </div>
        </td>
    </tr>
<?php
    $count++;
}
?>


</tbody>
</table>
<hr>
<footer></footer>
</div>

@endsection
