@extends('layouts.admin-app')
@section('content')

@if(count($errors))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <br/>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">

  
    <div class="page-header">
        <h1>Customer WishList Products</h1>

    </div>



    <br>
    <table id="testTable" class="table table-striped">
        <thead>
        <tr>
            <th>S. No.</th>
            <th>Customer Name</th>
            <th>Product Name</th>
            <th>Product Name</th>
        </tr>
        </thead>

        <tbody>

        <?php
        $count = 1;
        foreach($wishlist as $wishlistt)
        {
            ?>
            <tr>
                <td><?= $count; ?></td>
                <td class="gc_cell_left">
                    <?php
                    $user = App\User::where('id',$wishlistt->user_id)->first();
                    echo $user->name .' '.$user->lastname;
                    ?>
                </td>
                <td class="gc_cell_left">
                    <?php
                    $product = App\products::where('id',$wishlistt->product_id)->first();
                    echo $product->prod_name;
                    ?>
                </td>
                <td>
                    <i class="fa fa-heart"></i>
                </td>
            </tr>
            <?php
            $count++;
        }
        ?>


        </tbody>
    </table>
    <hr>
    <footer></footer>
</div>

@endsection
