@extends('layouts.admin-app')
@section('content')


@if (\Session::has('success'))
<div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
   <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif 


<div class="page-header">
	<h1>Brand Offers & Promotions</h1>
</div>
<a class="btn btn-primary" style="float:right;" href="{{ url('/admin/brand_offers/form') }}">
	<i class="icon-plus"></i> Add New Brand Offer
</a>
<table class="table">
    <thead>
        <tr>
        	<th>Offer Name</th>
        	<th>Brand Name</th>
           	<th>Enable Date</th>
          	<th>Disable Date</th>
          	<th>Reduction Type</th>
          	<th>Reduction Amount</th>
          	<th>Status</th>
          	<th></th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach($brand_wise_discounts as $discount)
    {
        ?>
        <tr>
            <td><?= $discount->offer_name; ?></td>
            <td>
                @php $show_name = App\Brands::where('id',$discount->brand_id)->first(); @endphp
                {{$show_name->brands_name}}
            </td>
            <td><?= $discount->enable_on_utc; ?></td>
            <td><?= $discount->disable_on_utc; ?></td>
            <td><?= $discount->reduction_type; ?></td>
            <td><?= $discount->percentage; ?></td>
            <td><?php if($discount->status == 1) { echo "Enabled"; } else { echo "Disabled"; } ?></td>
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default" href="{{ url('/admin/brand_offers/edit/') }}<?= '/'.$discount->id; ?>">
                        <i class="icon-pencil"></i>
                    </a>
                    <a class="btn btn-danger" href="{{ route('brands_offers_delete',$discount->id)}}" onclick="return confirm('are you sure?');">
                        <i class="icon-times"></i>
                    </a>
                </div>
            </td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>    
@endsection