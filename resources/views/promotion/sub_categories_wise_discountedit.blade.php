@extends('layouts.admin-app')
@section('content')
<div class="container">
    <script type="text/javascript">
        function areyousure(){
            return confirm('Are you sure you want to delete this category?');
        }
    </script>
    <style type="text/css">
        .pagination {
            margin:0px;
        }
    </style>
    <div class="page-header">
        <h1>Add Sub Category Wise Offers &amp; Promotions</h1>
    </div>
    <form action="{{ url('/admin/sub_category_wise_offers/formpostedit') }}" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <input type="hidden" value="<?= $category_wise_discounts->id; ?>" name="categorywisediscountid">

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="offer_name">Offer Name</label>
                    <input type="text" name="offer_name" value="<?= $category_wise_discounts->offer_name; ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="start_date">Enable On (UTC)</label>
                    <input type="text" name="start_date" value="<?= $category_wise_discounts->enable_on_utc; ?>" class="form_datetime form-control start_date" readonly="true">
                </div>
                <div class="form-group">
                    <label for="end_date">Disable On (UTC)</label>
                    <input type="text" name="end_date" value="<?= $category_wise_discounts->disable_on_utc; ?>" class="form_datetime form-control end_date" readonly="true">
                </div>
                <div class="form-group">
                    <label for="reduction_amount">Reduction Amount</label>
                    <div class="row">
                        <div class="col-md-6">
                            <select name="reduction_type" class="form-control">
                                <option value="<?= $category_wise_discounts->reduction_type; ?>" selected><?= $category_wise_discounts->reduction_type; ?></option>
                                <option value="percent">Percentage</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="reduction_amount" value="<?= $category_wise_discounts->percentage; ?>" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <div class="form-group">
                        <select name="enabled_1" class="form-control">
                            <option value="1"  {{$category_wise_discounts->status == 1 ? 'selected' : ''}}>Enabled</option>
                            <option value="0"  {{$category_wise_discounts->status == 0 ? 'selected' : ''}}>Disabled</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-md-offset-1 well pull-right">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Select Category</label>
                            <select class="form-control primary_category" name="primary_category" id="primary_category" required>
<!--                                <option value="">SELECT</option>-->
                                <?php
                                foreach($categories as $category)
                                {
                                    ?>
                                    <option value="<?= $category->id; ?>" <?php if($category->id == $category_wise_discounts->category_id) { echo "selected"; } ?>><?= $category->cat_name; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Select Sub Category</label>
                            <select class="form-control secondary_category" name="secondary_category" id="secondary_category" required>
<!--                                <option value="">SELECT</option>-->
                                <?php
                                $subcategories = App\subcategories::where('parent_cat_id',$category_wise_discounts->category_id)->get();
                                foreach($subcategories as $subcategory)
                                {
                                    ?>
                                    <option value="<?= $subcategory->id; ?>" <?php if($subcategory->id == $category_wise_discounts->sub_category_id) { echo "selected"; } ?>><?php echo $subcategory->sub_cat_name; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <a href="#" onclick="addsubcategory()" class="btn btn-primary" title="Add SubCategory">Add SubCategory</a>
                            </div>
                            <div class="col-md-12" id="selectedproducts">
                                <input type="hidden" name="promotionid" value="{{ $promotionid }}">
                                <input type="hidden" name="numberofsubcategory" id="numberofsubcategory" value="">
                                <h3>Selected Sub Categories</h3>
                                <?php
                                $count = 1;
                                foreach($promotionselectedproducts as $promotionselectedproduct)
                                {
                                    $subcategory = App\subcategories::where('id',$promotionselectedproduct->sub_category_id)->first();
                                    ?>
                                    <div class="col-md-12">
                                        <p style="display: inline"><?= $count; ?>. <?= $subcategory->sub_cat_name; ?></p>
                                        <input type="hidden" name="subcategory<?= $count; ?>" value="<?= $subcategory->id; ?>">
                                        <a style="padding: 2px 8px; cursor: pointer; float: right; background-color: red;" onclick="removesubcategory(<?= $subcategory->id; ?>)"><i class="icon-times"></i></a>
                                    </div>
                                    <?php
                                    $count++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <div class="col-md-2"></div>
        </div>
    </form>



    <hr>
    <footer></footer>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function(){
        $("#primary_category").change(function(){
            var $catId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getsubcategories') }}",
                data: {categoryid:$catId},
                success: function(result)
                {
                    $('#secondary_category').html(result);
                }
            });

        });
    });

    var subcategorycount = "<?php echo $count; ?>";

    var selectedsubcategoryids = [];

    function addsubcategory()
    {
        var subcategory = $("#secondary_category option:selected");
        var subcategoryvalue = subcategory.val();

        var subcategoryHtml = subcategory.html();
//
//        var productfound = false;

        for(var a = 0; a < selectedsubcategoryids.length; a++)
        {
            if(subcategoryvalue == selectedsubcategoryids[a])
            {
                alert('Sub Category Added');
                return false;
            }
        }
////
        subcategoryHtml = subcategorycount+". "+subcategoryHtml;
////
////        selectedproducts
        var nodediv = document.createElement("div");
        nodediv.setAttribute("class","col-md-12");
        nodediv.setAttribute("id","subcategorydivid"+subcategoryvalue);
//
        var node = document.createElement("p");                 // Create a <li> node
        var textnode = document.createTextNode(subcategoryHtml);
        node.appendChild(textnode);
        node.setAttribute("style","display: inline");
//
        var node1 = document.createElement("input");                 // Create a <li> node
        node1.setAttribute("type","hidden");
        node1.setAttribute("name","subcategory"+subcategorycount);
        node1.setAttribute("value",subcategoryvalue);

        nodediv.appendChild(node);
        nodediv.appendChild(node1);
////
////
        var atag = document.createElement("a");
        atag.setAttribute("style","padding: 2px 8px;cursor: pointer; float: right; background-color: red");
        var buttonfunction = "removesubcategory("+subcategoryvalue+")";
        atag.setAttribute("onclick",buttonfunction);
////
////
        var itag =  document.createElement("i");
        itag.setAttribute("class","icon-times");
        atag.appendChild(itag);
        nodediv.appendChild(atag);
////
////
        document.getElementById("selectedproducts").appendChild(nodediv);
//
        document.getElementById("numberofsubcategory").value = subcategorycount;
//
        subcategorycount++;
//
        selectedsubcategoryids.push(subcategoryvalue);
    }



    function removesubcategory(id)
    {
        $.ajax({
            type: "POST",
            url: "{{ url('admin/subcategorypromotionremovecategory') }}",
            data: {subcategoryid:id},
            success: function(result)
            {
                location.reload();
            }
        });
    }
</script>
@endsection