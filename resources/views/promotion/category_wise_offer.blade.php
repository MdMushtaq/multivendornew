@extends('layouts.admin-app')
@section('content')


@if (\Session::has('success'))
<div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
   <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif 


<div class="page-header">
	<h1>Category Wise Offers & Promotions</h1>
</div>
<a class="btn btn-primary" style="float:right;" href="#">
	<i class="icon-plus"></i> Add New Category Offer
</a>
<table class="table">
    <thead>
        <tr>
          <th>Offer Name</th>
          <th>Category Name</th>
          <th>Sub-Category Name</th>
          <th>Enable Date</th>
          <th>Disable Date</th>
          <th>Reduction Type</th>
          <th>Reduction Amount</th>
          <th>Status</th>
          <th></th>
        </tr>
    </thead>
    <tbody>



    </tbody>
</table>
    
@endsection