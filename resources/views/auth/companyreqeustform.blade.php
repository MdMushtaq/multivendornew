<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('public/app.name', 'TechnoSouq') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
</head>
<body>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="margin-top: 50px;">
                <img src="{{asset('public/logo.png')}}"  class="img-responsive" alt="Logo" style="margin-left: auto;margin-right:auto;display:block; max-width: 200px;">


                <div class="card-header text-center" style="font-size:30px;">Company Request Form</div>



                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">

                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    @if (\Session::has('companyid'))
                    <div class="alert alert-warning">
                        <center>
                            <p>Your account is available and non-approved You can request again</p>
                        </center>
                    </div>
                    @endif
                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <center>
                            <p>{{ \Session::get('success') }}</p>
                        </center>
                    </div>
                    @endif

                    <form method="POST" action="{{ url('/c/request') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="companyid" value="{{ \Session::has('companyid') }}">

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-lg" style="background-color: #375aa6;">
                                    Request Again
                                </button>

                                {{-- @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>


