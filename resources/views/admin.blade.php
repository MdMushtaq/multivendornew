@extends('layouts.admin-app')
@section('content')

	<!-- <div class="row"> -->
        <div class="page-header">
            <h1>Dashboard</h1>
        </div><br>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="tile tile-primary">
                    <div class="tile-heading">New Companiew
                    </div>
                    <div class="tile-body">
                        <i class="fa fa-shopping-cart"></i>                    
                        <h2 class="pull-right">{{count($companies)}}</h2>
                    </div>
                    <div class="tile-footer">
                        <a href="">View more...</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="tile tile-primary">
                    <div class="tile-heading">Total Orders
                    </div>
                    <div class="tile-body">
                        <i class="fa fa-shopping-cart"></i>                    
                        <h2 class="pull-right">0</h2>
                    </div>
                    <div class="tile-footer">
                        <a href="">View more...</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="tile tile-primary">
                    <div class="tile-heading">Total Sales
                    </div>
                    <div class="tile-body">
                        <i class="fa fa-credit-card"></i>
                         <h2 class="pull-right">  SR 0.00</h2>
                    </div>
                    <div class="tile-footer">
                        <a href="">View more...</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="tile tile-primary">
                    <div class="tile-heading">Total Customers
                    </div>
                    <div class="tile-body">
                        <i class="fa fa-user"></i>
                        <h2 class="pull-right">
                            <?= $totalcustomers; ?>
                        </h2>
                    </div>
                    <div class="tile-footer">
                        <a href="{{ url('admin/customers') }}">View more...</a>
                    </div>
                </div>
            </div>
        </div>
        <h2>Recent Orders</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Orders Serial</th>
                    <th>Orders Number</th>
                    <th>Date</th>
                    <th>Customer Name</th>
                    <th>Status</th>
                    <th>Total</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                     <td>
                         <b style="color:#337ab7;">306</b> 
                     </td>  
                    <td style="white-space:nowrap">
                        <strong>
                            <a href="#">7222</a>
                        </strong>
                    </td>
                    <td>02/11/19 01:29 pm</td>
                    <td>Kaleem Ahmed</td> 
                    <td>Confirmed By Admin</td>
                    <td>
                        <div>SR1,260.00</div>
                    </td>
                     <td class="text-right">
                        <div class="btn-group">
                            <a class="btn btn-default" href="#">
                                <i class="icon-eye"></i>
                            </a>
                        </div>
                     </td>
                   </tr>
                <tr>
                     <td> 
                         <b style="color:#337ab7;">305</b> 
                     </td>  
                    <td style="white-space:nowrap">
                        <strong>
                            <a href="#">7221</a>
                        </strong>
                    </td>
                    <td>02/11/19 01:19 pm</td>
                     <td>Kaleem Ahmed</td> 
                    <td>Confirmed By Admin</td>
                    <td>
                        <div>SR1,026.90</div>
                    </td>
                     <td class="text-right">
                        <div class="btn-group">
                            <a class="btn btn-default" href="#">
                                <i class="icon-eye"></i>
                            </a>
                        </div>
                     </td>
                </tr>
                <tr>
                     <td> <b style="color:#337ab7;">304</b> </td>  
                       <td style="white-space:nowrap">
                        <strong>
                            <a href="#">7210</a>
                        </strong>
                    </td>
                    <td>01/17/19 12:43 pm</td>
                       <td>sdf sdf</td> 
                    <td>Confirmed By Admin</td>
                    <td>
                        <div>SR120.96</div>
                    </td>
                     <td class="text-right">
                        <div class="btn-group">
                            <a class="btn btn-default" href="#">
                                <i class="icon-eye"></i>
                            </a>
                        </div>
                     </td>
                </tr>
                <tr>
                     <td> 
                         <b style="color:#337ab7;">303</b> 
                     </td>  
                    <td style="white-space:nowrap">
                        <strong>
                            <a href="#">7163</a>
                        </strong>
                    </td>
                    <td>11/22/18 02:06 pm</td>
                       <td>shahg</td> 
                    <td>Confirmed by Account</td>
                    <td>
                        <div>SR69.30</div>
                    </td>
                     <td class="text-right">
                        <div class="btn-group">
                            <a class="btn btn-default" href="#">
                                <i class="icon-eye"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                     <td> 
                         <b style="color:#337ab7;">302</b> 
                     </td>  
                    <td style="white-space:nowrap">
                        <strong>
                            <a href="#">6876</a>
                        </strong>
                    </td>
                    <td>11/17/18 03:15 pm</td>
                    <td>Abdul Muqee</td> 
                    <td>Confirmed By Admin</td>
                    <td>
                        <div>SR81.90</div>
                    </td>
                     <td class="text-right">
                        <div class="btn-group">
                            <a class="btn btn-default" href="#">
                                <i class="icon-eye"></i>
                            </a>
                        </div>
                     </td>
                </tr>
               </tbody>
        </table>
        <div class="row">
            <div class="col-md-12" style="text-align:center;">
                <a class="btn btn-primary" href="">View All Orders</a>
            </div>
        </div>
        <h2>Recent Customers</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Joining Date</th>
                    {{-- <th>Customer Id</th> --}}
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <th>Email </th>
                  </tr>
            </thead>
            <tbody>
            <?php
            foreach($customers as $customer)
            {
                ?>
                <tr>
                    <td><?= $customer->created_at; ?></td>
                    
                    <td class="gc_cell_left"><?= $customer->name; ?></td>
                    <td class="gc_cell_left"><?= $customer->phone; ?></td>
                    <td class="gc_cell_left"><?= $customer->email; ?></td>

                </tr>
                <?php
            }
            ?>

              
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12" style="text-align:center;">
                <a class="btn btn-primary" href="{{ url('admin/customers') }}">View All Customers</a>
            </div>
        </div>    
        <br>
        <br>
        <br>
@endsection
