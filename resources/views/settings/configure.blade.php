@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Configuration</h1>
</div>

<form action="#" enctype="multipart/form-data" method="post" accept-charset="utf-8">
 	<fieldset>
        <legend>Shop Details</legend>
        <div class="row">        
           	<div class="col-md-3">
            	<div class="form-group">         
            		<div style="text-align:center;height:100px; width: 100px; padding-left:5px; margin-left: 65px; border:1px solid #ddd;">
            			<img src="uploads/logo/ff0fd189b5d80c1ae08d40e34effc864.png" alt="current"><br>
            		</div>
      				<div class="input-append">
                		<input type="file" name="logo" class="form-control">
            		</div>
        		</div>
          	</div>
			 <div class="col-md-3">
                <div class="form-group">
                    <label>Shop Name</label>
                    <input type="text" name="company_name" value="صيدلية.كوم" class="form-control">
                </div>
            </div>
		   <div class="col-md-3">
                <div class="form-group">
                    <label>Theme</label>
                    <select name="theme" class="form-control">
						<option value="sadaliah" selected="selected">sadaliah</option>
					</select>
                </div>
            </div>
    		<div class="col-md-3">
                <div class="form-group">
                    <label>Select Homepage</label>
                    <select name="homepage" class="form-control">
						<option value="home" selected="selected">HOME</option>
					</select>
                </div>
            </div>
            <div class="col-md-3">
	            <div class="form-group">
	                <label>Default Meta Keywords</label>
	                <input type="text" name="default_meta_keywords" value="صيدلية" class="form-control">
	            </div>
        	</div>

	        <div class="col-md-3">
	            <div class="form-group">
	                <label>Default Meta Description</label>
	                <input type="text" name="default_meta_description" value="المنتجات الصيدلانية مهمةٌ للغاية، ولا يمكن لأي شخص أن ينكر ذلك؛ وحرصًا منَّا على راحتكم أطلقنا موقع صيدلية.كوم؛" class="form-control">
	            </div>
	        </div>
        	<div class="col-md-3">
                <div class="form-group">
                    <label>Products per Category Page</label>
                    <input type="text" name="products_per_page" value="24" class="form-control">
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Home Page</legend>
        <div class="col-md-3">
            <div class="form-group">
                <label>HomePage Banner</label>
                <select class="form-control" name="banner">
                    <option selected="" value="3">Home page banner</option>
                    <option value="4">Category page Banner</option>
                </select>
            </div>
        </div>
		<div class="col-md-3">
            <div class="form-group">
                <label>Category Banner</label>
                <select class="form-control" name="banner_category">
                    <option value="3">Home page banner</option>
                    <option selected="" value="4">Category page Banner</option>
                </select>
            </div>
       </div>
		<div class="col-md-3"> 
            <div class="form-group">
                <label>Trending</label>
                <select class="form-control" name="trending">
                   	<option selected="selected" value="">--Select trending--</option>
                    <option value="">child testing</option>
                    <option value="">Offer</option>
                </select>                    
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Popular</label>
                <select class="form-control" name="popular">
                	<option selected="selected" value="">--Select popluler--</option>
                	<option value="">child testing</option>
                    <option value="">Offer</option>
       			</select>                    
       		</div>  
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Phone</label>
                <input type="text" name="phone" value="920008705" class="form-control">
           	</div>
        </div>
        <div class="col-md-4">
           	<div class="form-group">
                <label>Web</label>
               	<input type="text" name="welcome" value="Welcome to saidaliah.com" class="form-control">
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Email Settings</legend>
        <div class="row form-group">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Email To Address</label>
                    <input type="text" name="email_to" value="info@saidaliah.com" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Email From Address</label>
                    <input type="text" name="email_from" value="team@saidaliah.com" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Email Method</label>
                    <select name="email_method" class="form-control" id="emailMethod">
						<option value="mail" selected="selected">Mail</option>
					</select>
                </div>
            </div>
        </div>
        <div class="row emailMethods form-group" id="email_smtp">
            <div class="col-md-3">
                <div class="form-group">
                <label>SMTP Server</label>
                <input type="text" name="smtp_server" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>SMTP Port</label>
                    <input type="text" name="smtp_port" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>SMTP Username</label>
                    <input type="text" name="smtp_username" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>SMTP Password</label>
                    <input type="text" name="smtp_password" value="" class="form-control">
                </div>
            </div>
        </div>
        <div class="row emailMethods" id="email_sendmail">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Sendmail Path</label>
                    <input type="text" name="sendmail_path" value="" class="form-control">
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Ship From Address</legend>
        <div class="row">
        	<div class="col-md-4">
        		<div class="form-group">
            		<label>Country</label>
            		<select name="country_id" id="country_id" class="form-control">
						<option value="17">Bahrain</option>
						<option value="64">Egypt</option>
					</select>
        		</div>
       		</div>
	       	<div class="col-md-4">   
	        	<div class="form-group">
	            	<label>Address</label>
	            	<input type="text" name="address1" value="Madina Road" class="form-control">
	        	</div>
	    	</div>
	        <div class="col-md-4">
	        	<div class="form-group">
	            	<label>Address</label>
	            	<input type="text" name="address2" value="" class="form-control">
	        	</div>
	    	</div>
		</div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>City</label>
                    <input type="text" name="city" value="Jeddah" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>State / Province</label>
                    <select name="zone_id" id="zone_id" class="form-control">
						<option value="">Al Khobar</option>
						<option value="">Aseer</option>
						<option value="">Ash Sharqiyah</option>
					</select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Zip / Postcode</label>
                    <input type="text" name="zip" value="" maxlength="10" class="form-control">
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Social Media</legend>
       	<div class="row">        
           	<div class="col-md-4">
              	<div class="form-group">
                   <label>Facebook</label>
                   <input type="text" name="" value="f" class="form-control">
              	</div>
          	</div>
          	<div class="col-md-4">
              	<div class="form-group">
                   <label>Youtube</label>
                   <input type="text" name="" value="" class="form-control">
              	</div>
          	</div>   
          	<div class="col-md-4">
              	<div class="form-group">
                   <label>twitter</label>
                   <input type="text" name="" value="" class="form-control">
              	</div>
          	</div>  
          	<div class="col-md-4">
              	<div class="form-group">
                   <label>Viemo</label>
                   <input type="text" name="" value="" class="form-control">
              	</div>
          	</div>   
          	<div class="col-md-4">
              	<div class="form-group">
                   <label>Linkedin</label>
                   <input type="text" name="" value="" class="form-control">
              	</div>
          	</div>         
       	</div>
    </fieldset>

    <fieldset>
        <legend>Copy Right</legend>
         <div class="row">        
           <div class="col-md-4">
              <div class="form-group">
                   <label>copyright</label>
                   <input type="text" name="copyright" value="Copyright 2018 by Saidaliah.com" class="form-control">
              </div>
          </div>
        </div>
    </fieldset>
   

    <fieldset>
        <legend>Locale &amp; Currency</legend>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Locale</label>
                    <select name="locale" class="form-control">
						<option value="af">Afrikaans</option>
						<option value="af_NA">Afrikaans (Namibia)</option>
						<option value="af_ZA">Afrikaans (South Africa)</option>
					</select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Timezone</label>
                    <select name="timezone" class="form-control">
						<option value="Africa/Abidjan">Africa/Abidjan</option>
						<option value="Africa/Accra">Africa/Accra</option>
						<option value="Africa/Addis_Ababa">Africa/Addis_Ababa</option>
					</select>
                </div>
            </div>
           	<div class="col-md-4">
                <label>Currency</label>
                <div class="form-group">
	                <select name="currency_iso[]" ["gbp","sar","usd"]="" multiple="multiple">
						<option value="AED">AED</option>
						<option value="AFN">AFN</option>
						<option value="ALL">ALL</option>
					</select>
                </div>
            </div>
       </div>
    </fieldset>

    <fieldset>
        <legend>Security</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>DEV Username</label>
                    <input type="text" name="stage_username" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>DEV Password</label>
                    <input type="text" name="stage_password" value="" class="form-control">
                </div>
            </div>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="ssl_support" value="" checked="checked">Use SSL
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="require_login" value="">Require login to checkout.
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="new_customer_status" value="" checked="checked">New customers are automatically activated.
            </label>
        </div>
    </fieldset>

    <fieldset class="hidden">
        <legend>Package Details</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Weight Unit (LB, OZ, KG, etc.)</label>
                    <input type="text" name="weight_unit" value="KG" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Dimensional Unit (FT, IN, CM, etc.)</label>
                    <input type="text" name="dimension_unit" value="CM" class="form-control">
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Orders &amp; Inventory</legend>
        <table class="table">
            <thead>
                <tr>
                    <th style="width:20%;">Default Order Status</th>
                    <th style="width:20%;">Order Status Options</th>
                    <th style="text-align:right;">
                    	<div class="col-md-4 input-group pull-right">
                        	<input type="text" value="" class="form-control" id="new_order_status_field" style="margin:0px;" placeholder="Status Name">
                        	<div class="input-group-btn">
	                        	<button type="button" class="btn btn-success" onclick="">
	                        		<i class="icon-plus"></i>
	                        	</button>
                        	</div>
                        </div>
                    </th>
                </tr>
            </thead>
            <tbody id="orderStatuses">
            	<tr>
			        <td>
			        	<input type="radio" value="Confirmed By Admin" name="order_status">
			        </td>
			        <td>
			            Confirmed By Admin
			        </td>
			        <td style="text-align:right;">
			            <button type="button" class="removeOrderStatus btn btn-danger" value="Confirmed By Admin">
			            	<i class="icon-close"></i>
			            </button>
			        </td>
    			</tr>
			    <tr>
			        <td>
			            <input type="radio" value="Confirmed by Account" name="order_status">
			        </td>
			        <td>
			            Confirmed by Account
			        </td>
			        <td style="text-align:right;">
			            <button type="button" class="removeOrderStatus btn btn-danger" value="Confirmed by Account">
			            	<i class="icon-close"></i>
			            </button>
			        </td>
			    </tr>
			    <tr>
			        <td>
			            <input type="radio" value="Confirmed by Delivery Depart" name="order_status">
			        </td>
			        <td>
			            Confirmed by Delivery Depart
			        </td>
			        <td style="text-align:right;">
			            <button type="button" class="removeOrderStatus btn btn-danger" value="Confirmed by Delivery Depart">
			            	<i class="icon-close"></i>
			            </button>
			        </td>
			    </tr>
			    <tr>
			        <td>
			            <input type="radio" value="Confirmed by Warehouse" name="order_status">
			        </td>
			        <td>
			            Confirmed by Warehouse
			        </td>
			        <td style="text-align:right;">
			            <button type="button" class="removeOrderStatus btn btn-danger" value="Confirmed by Warehouse">
			            	<i class="icon-close"></i>
			            </button>
			        </td>
			    </tr>
			</tbody>
        </table>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="inventory_enabled" value="" checked="checked">Enable Inventory Tracking
            </label>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="allow_os_purchase" value="1" checked="checked">Allow customers to buy items that are out of stock
            </label>
        </div>
    </fieldset>
    <fieldset>
        <legend>Delivery Settings</legend>
        <div class="form-group">
            <label>Base Delivery charges of which address?</label>
            <select name="tax_address" class="form-control">
				<option value="" selected="selected">Shipping Address</option>
				<option value="">Billing Address</option>
			</select>
        </div>
        <div class="checkbox hidden">
            <label>
                <input type="checkbox" name="tax_shipping" value="1" checked="checked">Charge Delivery on shipping rates
            </label>
        </div>
    </fieldset>
    <input type="submit" class="btn btn-primary" value="Save">
</form>
@endsection