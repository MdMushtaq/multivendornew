@extends('layouts.admin-app')
@section('content')


@if (\Session::has('success'))
<div class="alert alert-success">
    <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif

<div class="page-header"><h1>Subscriptions</h1></div>

<div style="text-align:right">
    <a href="{{ url('/admin/generateexcelsheet') }}">
        <button class="btn btn-primary">Export to Excel</button>
    </a>
    <a href="{{ url('/admin/generatetonotepad') }}">
        <button class="btn btn-primary">Export to Notepad</button>
    </a>
</div>

<table class="table table-striped">
    <thead>
    <tr>
        <th>S.No</th>
        <th>Email</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    @php
    $count = 1;
    @endphp
    @foreach($subscriptions as $subscription)
    <tr>
        <td>{{ $count }}</td>
        <td>{{ $subscription->email }}</td>
        <td>{{ $subscription->created_at }}</td>
    </tr>
    @php
    $count++;
    @endphp
    @endforeach
    </tbody>
</table>

@endsection
