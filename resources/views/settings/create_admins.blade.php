@extends('layouts.admin-app')
@section('content')
<style type="text/css">
    input[type=checkbox], input[type=radio] {
        margin: 17px 4px 5px;
        margin-top: 1px 9px;
        display: inline-block;
        line-height: normal;
    }
</style>

<div class="page-header">
	<h1>Add New Administrator</h1>
</div>


@if(count($errors))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <br/>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>

</div>

@endif

<form action="{{route('store.admins')}}" method="post" accept-charset="utf-8">
    @csrf
	<div class="row">
	
		<div class="panel panel-default">
	    	<div class="panel-body">
	    		<div class="row">
	            	<div class="col-md-3">
	                	<div class="form-group">
	                    	<label>Full Name</label>
	                    	<input type="text" name="name" value="" class="form-control"  />
	                	</div>
	            	</div>
		            {{-- <div class="col-md-3">
		                <div class="form-group">
		                    <label>Last Name</label>
		                    <input type="text" name="lastname" value="" class="form-control"  />
		                </div>
		            </div>
		            <div class="col-md-3">
		                <div class="form-group">
		                    <label>Username</label>
		                    <input type="text" name="username" value="" class="form-control"  />
		                </div>
		            </div> --}}
		            <div class="col-md-3">
		                <div class="form-group">
		                    <label>Email</label>
		                    <input type="text" name="email" value="" class="form-control"  />
		                </div>
		            </div>
	        
	        	
	            	{{-- <div class="col-md-3">
	                	<div class="form-group">
	                    	<label>Access</label>
		                    <select name="access" class="form-control">
								<option value="Admin">Administrator</option>
								<option value="account">Account Department</option>
							</select>
	                	</div>
	            	</div> --}}
		            <div class="col-md-3">
		                <div class="form-group">
		                    <label>Password</label>
		                    <input type="password" name="password" value="" class="form-control"  />
		                </div>
		            </div>
		            <div class="col-md-3">
		                <div class="form-group">
		                    <label>Confirm Password</label>
		                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" />
		                </div>
		            </div>
                </div>
	    	</div>
		</div>
		<h2>User Permission</h2>
			<div class="panel panel-default">
	    		<div class="panel-body">
					<div class='row'>
                        @foreach ($permissions as $permission)
                            <div class='col-md-4'>    
                                <li>
                                    <input class='checkbox' type='checkbox' name='permissions[{{$permission->id}}]'>
                                    <strong>{{ $permission->name }}</strong>
                                    <ul>
                                        @foreach ($permission->children as $child)
                                            <li>
                                                <input class='checkbox' type='checkbox' name='permissions[{{$child->id}}]'>
                                                <strong>{{ $child->name }}</strong>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>	 
                            </div>
                        @endforeach
	        		</div>
	        	</div> 
			</div>
			<div class="row">
	    		<div class="col-md-10">
	        		<input class="btn btn-primary" type="submit" value="Save"/>
	    		</div>
	    	<div class="col-md-2"></div>
		</div>
	</div>
</form>
@endsection