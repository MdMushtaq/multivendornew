@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Countries</h1>
</div>
<div class="text-right">
    <a class="btn btn-primary" href="add-new-country.php">
        <i class="icon-plus"></i> Add New Country
    </a>
    <a class="btn btn-primary" href="add-new-zone.php">
        <i class="icon-plus"></i> Add New Zone
    </a>
</div>

<table class="table table-striped" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th>Sort</th>
            <th>Name</th>
            <th>ISO Code 2</th>
            <th>ISO Code 3</th>
            <th>Cash on Develivery Charges:</th>
            <th>Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody id="countries" class="ui-sortable">
        <tr id="country-1">
            <td class="handle">
                <a class="btn btn-primary">
                    <span class="icon-sort"></span>
                </a>
            </td>
            <td>Afghanistan</td>
            <td>AF</td>
            <td></td>
            <td>0</td>
            <td>enabled</td>
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default" href="#">
                        <i class="icon-pencil"></i>
                    </a>
                    <a class="btn btn-default" href="#">
                        <i class="icon-map-marker"></i>
                    </a>
                    <a class="btn btn-danger" href="#" onclick="">
                        <i class="icon-times"></i>
                    </a>
                </div>
            </td>
        </tr>
        <tr id="country-2">
            <td class="handle">
                <a class="btn btn-primary">
                    <span class="icon-sort"></span>
                </a>
            </td>
            <td>Albania</td>
            <td>AL</td>
            <td></td>
            <td>0</td>
            <td>enabled</td>
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default" href="#">
                        <i class="icon-pencil"></i>
                    </a>
                    <a class="btn btn-default" href="#">
                        <i class="icon-map-marker"></i>
                    </a>
                    <a class="btn btn-danger" href="#" onclick="">
                        <i class="icon-times "></i>
                    </a>
                </div>
            </td>
        </tr>
    </tbody>
</table>
@endsection