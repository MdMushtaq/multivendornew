@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Administrators</h1>
</div>
<div class="text-right">
    <a class="btn btn-primary" href="{{route('create.admins')}}">
    	<i class="icon-plus"></i> Add New Admin
    </a>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th>Full Name</th>
            <th>Email</th>
            <th>Access</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($admins as $administrator)
        <tr>
            <td>{{ $administrator->name}}</td>
            <td>{{ $administrator->email}}</td>
            <td>{{ $administrator->job_title}}</td>
            {{-- <td>Admin</td> --}}

            @if ($administrator->id != 1)
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default" href="#">
                    	<i class="icon-pencil"></i>
                    </a> 
                    <a class="btn btn-danger" href="#">
                    	<i class="icon-times"></i>
                    </a> 
                </div>
            </td>
            @endif
           
        </tr>
        @endforeach
        
       
    </tbody>
</table>
@endsection