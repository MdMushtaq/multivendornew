@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Payment Modules</h1>
</div>
<table class="table table-striped">
    <tbody>
        <tr>
        	<td>Bank Transfer</td>
            <td>
                <span class="btn-group pull-right">
                    <a class="btn btn-default" href="#">
                       	<i class="icon-gear"></i>
                    </a>
                    <a class="btn btn-danger" href="#" onclick="">
                    	<i class="icon-times"></i>
                    </a>
                </span>
            </td>
        </tr>
        <tr>
        	<td>Telr</td>
            <td>
                <span class="btn-group pull-right">
                    <a class="btn btn-default" href="#">
                       	<i class="icon-gear"></i>
                    </a>
                    <a class="btn btn-danger" href="#" onclick="">
                    	<i class="icon-times"></i>
                    </a>
                </span>
            </td>
        </tr>
         <tr>
        	<td>Charge on Delivery</td>
            <td>
                <span class="btn-group pull-right">
                    <a class="btn btn-default" href="#">
                       	<i class="icon-gear"></i>
                    </a>
                    <a class="btn btn-danger" href="#" onclick="">
                    	<i class="icon-times"></i>
                    </a>
                </span>
            </td>
        </tr>
	</tbody>
</table>
@endsection