@extends('layouts.admin-app')
@section('content')


@if (\Session::has('success'))
<div class="alert alert-success">
    <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif

<div class="page-header"><h1>Faqs</h1></div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                &nbsp;
            </div>
            <div class="col-md-8">
                <form action="" class="form-inline form-group" style="float:right" method="post" accept-charset="utf-8">
                    @csrf
                    <div class="form-group">
                        <input type="text" id="myInput" onkeyup="myFunction()" class="form-control" name="catname" value="" placeholder="">
                    </div>
                    <!--                            <button class="btn btn-default" type="submit" name="submit" value="search">Search</button>-->
                    <a class="btn btn-default" href="#">Reset</a>
                </form>
            </div>
        </div>
    </div>
</div>

<div style="text-align:right">
    <a class="btn btn-primary" href="{{ url('admin/faq') }}">Faq</a>
    <a class="btn btn-primary" href="{{ url('admin/faq/category') }}">Faq Categories</a>
</div>

<table class="table table-striped" id="myTable">
    <thead>
    <tr>
        <th>Category Name</th>
        <th>Category Arabic Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach($faqcategories as $faqcategory)
    <tr>
        <td>{{ $faqcategory->category_name }}</td>
        <td>{{ $faqcategory->arabiccategoryname }}</td>


        <td>
            <div class="btn-group">

                <a class="btn btn-default" href="{{route('edit.faqcategory',$faqcategory->id)}}"><i class="icon-pencil"></i></a>


                <a class="btn btn-danger" href="{{route('edit.faqcategory',$faqcategory->id)}}" onclick="return confirm('are you sure you wan to delete?')"><i class="icon-times"></i></a>
            </div>
        </td>
    </tr>
    @endforeach



    </tbody>
</table>
<script>
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

</script>
@endsection
