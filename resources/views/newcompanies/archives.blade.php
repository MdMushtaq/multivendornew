@extends('layouts.admin-app')
@section('content')
@if (\Session::has('success'))
<div class="alert alert-success">
    <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif
<div class="page-header">
    {{-- <h1>{{$trash->status = 0 ? 'Deleted prduct' : 'Products' }}}</h1> --}}
</div>


<table  id="myTable" class="table table-striped">
    <thead>
    <tr>
        <th>Company Logo</th>
        <th>Company Name</th>
        <th>Person Name</th>
        <th>Mobile</th>
        <th>Email</th>
        <th style="width:16%"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($newcompanies as $newcompany)
    <tr>
        <td>
            <img src="{{ url('/public/images',$newcompany->companylogo) }}" width="30px" height="30px" alt="">
        </td>
        <td>{{ $newcompany->companyname }}</td>
        <td>{{ $newcompany->personname }}</td>
        <td>{{ $newcompany->mobile }}</td>
        <td>{{ $newcompany->email }}</td>
        <td class="text-right">
            <div class="btn-group">
                <a class="btn btn-default" href="{{ url('admin/newcompanies/edit',$newcompany->id) }}" alt="Edit">
                    <i class="icon-pencil"></i>
                </a>


                <!--                    <a class="btn btn-danger" href="{{ route('companies.delete',$newcompany->id) }}" onClick="confirm('Are your sure You Want to delete this product?')" alt="Delete">-->
                <!--                        <i class="icon-times"></i>-->
                <!--                    </a>-->
            </div>
        </td>
    </tr>

    @endforeach
    </tbody>
</table>

<script>
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

</script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function(){
        $("#category_filter").change(function(){
            var $catId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getsubcategories') }}",
                data: {categoryid:$catId},
                success: function(result)
                {
                    $('#subcategory_filter').html(result);
                }
            });

        });
    });
</script>

@endsection
