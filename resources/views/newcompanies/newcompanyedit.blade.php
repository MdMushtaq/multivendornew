@extends('layouts.admin-app')
@section('content')

<div class="container">

    <!--          <h1>Good</h1>-->
    <!--          <h1>Yes</h1>-->

    <div class="page-header"><h1>Company Form Edit</h1></div>

     
    <form action="{{ url('/admin/newcompanies/formeditpost') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <input type="hidden" name="newcompanyid" value="<?= $newcompany->id; ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Company Name
                            </label>
                            <input type="text" name="companyname" value="{{ $newcompany->companyname }}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Person Name                    </label>
                            <input type="text" name="personname" value="{{ $newcompany->personname }}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
           
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mobile</label>
                            <input type="text" name="mobile" value="{{ $newcompany->mobile }}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="{{ $newcompany->email }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="address" value="{{ $newcompany->address }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>City</label>
                            <input type="text" name="city" value="{{ $newcompany->city }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>License Number</label>
                            <input type="text" name="licensenumber" value="{{ $newcompany->licensenumber }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Company Logo</label>
                            <input type="file" name="companylogo" value="{{ $newcompany->licensenumber }}" class="form-control">
                            <br>
                            <img src="{{ url('/public/images',$newcompany->companylogo) }}" width="100px" height="100px" alt="">
                        </div>
                    </div>


                </div>




            </div>
      
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="form-group">
<!--                    <form class="form-inline" action="{{ url('/admin/newcompanies/accept') }}" method="post">-->
<!--                        @csrf-->
<!--                        <input type="hidden" name="newcompanyid" value="{{ $newcompany->id }}">-->
<!--                        <button class="btn btn-success" type="submit" value="accept">-->
<!--                            <i class="fa fa-check"></i> Accept</a>-->
<!--                        </button>-->
<!--                    </form>-->
                    <a class="btn btn-success" href="{{ url('/admin/newcompanies/accept',$newcompany->id) }}">
                        <i class="fa fa-check"></i> Accept</a>
                    </a>
                    <a class="btn btn-danger" href="{{ url('/admin/newcompanies/notaccept',$newcompany->id) }}" style="font-weight:normal;" href=""><i class="fa fa-close"></i> Not Accept</a>
                    <a class="btn btn-warning" style="font-weight:normal;" href="{{ url('/admin/newcompanies/archive',$newcompany->id) }}"><i class="fa fa-archive"></i> Archive</a>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <button type="submit" class="btn btn-primary">
                    Save        </button>
            </div>
            <div class="col-md-2">
            </div>
        </div>


    </form>



    <script type="text/javascript">
        $('form').submit(function() {
            $('.btn .btn-primary').attr('disabled', true).addClass('disabled');
        });
    </script>    <hr>
    <footer></footer>
</div>

@endsection
