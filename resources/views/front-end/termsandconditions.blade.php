@extends('layouts.front-app')
@section('content')
<style>
    .c1 h1
    {
        color: #39509c;
        font-family: sans-serif;
    }
    .c1 p
    {
        color: black;
        font-family: sans-serif;
    }
    h2
    {
        color: #39509c;
        font-family: sans-serif;
        font-size: 24px;
    }
    label
    {
        color: black;
    }
</style>
<div class="container">
    <div class="c1" style="background: #fff;">
        <h1>{{ trans('termsandconditions.TechnoSouq Terms and Conditions') }}</h1>
        <h2>{{ trans('termsandconditions.Terms and conditions of use') }}</h2>
        <p><b>{{ trans('termsandconditions.Dear User of TechnoSouq Applications and Website:') }}</b></p>
        <p><b>{{ trans('termsandconditions.Using TechnoSouq website and all you rpurchases and useof the products available on this site are subject to a set of terms and conditions. By reading and completing your purchase throughTechnoSouq website and its mobileapplications, you acknowledge and approve all the written terms and conditions.Theyare as follows:') }}</b></p>
        <p>{{ trans('termsandconditions.•	TechnoSouq has the right to accept,decline or cancel your order in certain cases. For example: if the product you wish to purchase is not available, if the product is priced incorrectly or if the order is found to be fraudulent. TechnoSouq will return what you have paid for orders that have not been accepted or cancelled.') }}</p>
        <p>{{ trans('termsandconditions.•	All content onTechnoSouq  website including: writings, designs, drawings, logos, icons, images, audio clips, downloads, interfaces, icons, codes and software; as well as, how to select and arrange) is an exclusive property owned by TechnoSouq and is subject to copyright and trademark protection.') }}</p>
        <p>{{ trans('termsandconditions.•	By purchasing or sending an email toTechnoSouq, you agree to receive any emails, notices and alerts from TechnoSouq.') }}</p>
        <p>{{ trans('termsandconditions.•	TechnoSouq reserves the right to make any modifications or changes to its website and to the policies and agreements associated with TechnoSouq, including: Privacy Policy, Terms and Conditions Document, Replacement and Return Policy, Shipping and Delivery Policy, and Payment Methods Policy.') }}</p>

    </div>
</div>
@endsection