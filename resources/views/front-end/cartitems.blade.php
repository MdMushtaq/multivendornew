@extends('layouts.front-app')
@section('content')

<div class="container">
    <br>
    <h3>Order Items</h3>
<table class="table">
    <tbody class="table table-striped">
        @foreach ($itemorder as $order)
            @foreach ($order->orderitems as $item)
                <tr>
                    <td>
                    <strong>{{$item->product_name}}</strong> <br>

                    </td>
                <td></td>
                    <td>
                        <div style="font-size:11px; color:#bbb;">({{$item->product_quantity}}  ×  {{$item->product_unit_price}})</div>SR {{ number_format($item->product_price, 2)}}
                    </td>
                </tr>
            @endforeach

        @endforeach
       
    </tbody>
   
    <tbody class="orderTotals">
        <tr>
        <td colspan="2">
            <div style="font-size:17px;">Total</div>
        </td>
            <td colspan="2">
                <div style="font-size:17px;">SR {{ number_format($order->total, 2) }}</div>
            </td>
        </tr>
      	<tr>
            <td colspan="2" class="cartSummaryTotalsKey tex">VAT :</td>
            <td class="cartSummaryTotalsValue ">SR {{number_format($order->vat, 2)}}</td>
        </tr>
        <tr>
            <tr>
                <td colspan="2">Subtotal</td>
                <td>SR {{ number_format($order->subtotal, 2)}}</td>
            </tr>
    </tbody>
</table>
</div>

@endsection