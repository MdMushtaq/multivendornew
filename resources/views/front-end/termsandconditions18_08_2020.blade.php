@extends('layouts.front-app')
@section('content')
<style>
    .c1 h1
    {
        color: #39509c;
        font-family: sans-serif;
    }
    .c1 p
    {
        color: black;
        font-family: sans-serif;
    }
    h2
    {
        color: #39509c;
        font-family: sans-serif;
        font-size: 24px;
    }
    label
    {
        color: black;
    }
</style>
<div class="container">
    <div class="c1" style="background: #fff;">
        <h1>{{ trans('termsandconditions.Terms and Conditions') }}</h1>
        <p>{{ trans('termsandconditions.Welcome to Technosouq. Technosouq Services LLC and/or its affiliates ("Technosouq") provide website features and other products and services to you when you visit or shop at Technosouq, use Technosouq products or services, use Technosouq applications for mobile, or use software provided by Technosouq in connection with any of the foregoing (collectively, "Technosouq Services"). Technosouq provides the Technosouq Services subject to the following conditions.') }}</p>
        <h2>{{ trans('termsandconditions.By using Technosouq Services, you agree to these conditions. Please read them carefully.') }}</h2>
        <p>{{ trans('termsandconditions.We offer a wide range of Technosouq Services, and sometimes additional terms may apply. When you use an Technosouq Service (for example, Your Profile, Gift Cards, Technosouq Video, Your Media Library, Technosouq devices, or Technosouq applications) you also will be subject to the guidelines, terms and agreements applicable to that Technosouq Service ("Service Terms"). If these Conditions of Use are inconsistent with the Service Terms, those Service Terms will control.') }}</p>
        <h2>{{ trans('termsandconditions.PRIVACY') }}</h2>
        <p>{{ trans('termsandconditions.Please review our Privacy Notice, which also governs your use of Technosouq Services, to understand our practices.') }}</p>
        <h2>{{ trans('termsandconditions.COPYRIGHT') }}</h2>
        <p>{{ trans('termsandconditions.All content included in or made available through any Technosouq Service, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software is the property of Technosouq.') }}</p>
    </div>
</div>
@endsection