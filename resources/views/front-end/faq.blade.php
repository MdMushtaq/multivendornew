@extends('layouts.front-app')

@section('content')
<style>
    .c1
    {
        background-color: #e3e8f7;
        padding-left: 10px;
        padding-bottom: 5px;
    }
    .c2
    {
        background-color: #e8eae8;
        /*border: 1px solid black;*/
        margin-bottom: 10px;
    }
    .c3
    {
        padding-left: 10px;
        font-family: sans-serif;
        background-color: #a3aad2;
        color: #252320;
    }
    .c4
      {
          font-family: sans-serif;
          font-size: 15px;
        color: black;
      }
</style>
<div class="container">
    <form action="{{ url('/faq') }}" method="POST">
        @csrf
    <div class="row">
        <div class="col-md-2">
            <a href="{{ url('/faq') }}">
                <h1>{{ trans('faq.Faq') }}</h1>
            </a>
        </div>

        <div class="col-md-3">
            <select name="faqcategoryid" class="form-control" style="margin-top: 5px;" required>
                <option value="">--{{ trans('faq.Select Faq Category') }}--</option>
                @foreach($faqcategories as $faqcategory)
                <option value="{{ $faqcategory->id }}">
                    @if(\Session::get('locale') == 'ar')
                    {{ $faqcategory->arabiccategoryname }}
                    @else
                    {{ $faqcategory->category_name }}
                    @endif
                    </option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
                <div class="input-group mb-3" style="margin-top: 5px;">
                    <input type="text" class="form-control" placeholder="{{ trans('faq.Search Frequently Ask Question') }}" name="question">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit">{{ trans('faq.Search') }}</button>
                    </div>
                </div>
        </div>

        <div class="col-md-3" style="padding-left: 69px;">
            <button style="margin-top: 5px;" class="btn">{{ trans('faq.Ask Question?') }}</button>
        </div>
    </div>
    </form>

<div class="panel-group" id="accordion">
    @foreach($faqs as $faq)
    <div class="panel panel-default c2">
        <div class="panel-heading">
            <h4 class="panel-title c1">
                <a class="c4" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $faq->id }}">
                    @if(\Session::get('locale') == 'ar')
                    {{ $faq->arabicquestion }}
                    @else
                    {{ $faq->question }}
                    @endif
                </a>
            </h4>
        </div>
        <div id="collapse{{ $faq->id }}" class="panel-collapse collapse in">
            <div class="panel-body c3">
                @if(\Session::get('locale') == 'ar')
                {{ $faq->arabicanswer }}
                @else
                {{ $faq->answer }}
                @endif
            </div>
        </div>
    </div>
    @endforeach

</div>
</div>
@endsection