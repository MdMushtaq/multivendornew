@extends('layouts.front-app')
@section('content')
<style>
    .c1 h1
    {
        color: #39509c;
        font-family: sans-serif;
    }
    .c1 p
    {
        color: black;
        font-family: sans-serif;
    }
    h2
    {
        color: #39509c;
        font-family: sans-serif;
        font-size: 24px;
    }
    label
    {
        color: black;
    }
    c1 a
    {
        color: blue;
    }
</style>
<div class="container">
    <div class="c1" style="background: #fff;">
        <h1>{{ trans('about.About Us')}}</h1>
        <p>{{ trans('about.Technosouq is guided by four principles: customer obsession rather than competitor focus, passion for invention, commitment to operational excellence, and long-term thinking.')}}</p>
        <h2>{{ trans('about.Bulding the future')}}</h2>
        <p>{{ trans('about.We strive to have a positive impact on customers, employees, small businesses, the economy, and communities. Amazonians are smart, passionate builders with different backgrounds and goals, who share a common desire to always be learning and inventing on behalf of our customers.')}}</p>
        <h2>{{ trans('about.What we do')}}</h2>
        <h2>{{ trans('about.Who we are') }}</h2>
        <p>{{ trans('about.Explore Technosouq’s products and services.') }}</p>
        <a style="color: #007bff" href="url('/')">{{ trans('about.Technosouq') }}</a>
        <br>
        <br>
    </div>
</div>
@endsection