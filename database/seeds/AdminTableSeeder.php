<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('admins')->delete();
        
        \DB::table('admins')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Master admin',
                'email' => 'master@gmail.com',
                'job_title' => 'master admin',
                'password' => '$2y$10$CJoRYywIBLzNACAqxaays.tCY/6thBma65pbmn0KKq03FbjUx7tFy',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'job_title' => 'admin',
                'password' => '$2y$10$CJoRYywIBLzNACAqxaays.tCY/6thBma65pbmn0KKq03FbjUx7tFy',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'subadmin',
                'email' => 'subadmin@gmail.com',
                'job_title' => 'subadmin',
                'password' => '$2y$10$CJoRYywIBLzNACAqxaays.tCY/6thBma65pbmn0KKq03FbjUx7tFy',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));

       
    }
}
