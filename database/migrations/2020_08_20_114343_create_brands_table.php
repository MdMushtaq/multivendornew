<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brands_name')->nullable();
            $table->string('brands_arb_name')->nullable();
            $table->string('brands_img')->nullable();
            $table->string('brands_status')->nullable();
            $table->string('brands_seo_title')->nullable();
            $table->string('brands_meta_data')->nullable();
            $table->string('brands_slug')->nullable();
            $table->string('brands_sequence')->nullable();
            $table->string('company_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
}
